#!/usr/bin/env python

import sys
import binascii

if sys.version_info.major == 2:
    # We are using Python 2.x
    lt_3 = True
    import Tkinter as tk
    from tkFont import Font
elif sys.version_info.major == 3:
    # We are using Python 3.x
    lt_3 = False
    import tkinter as tk
    from tkinter.font import Font

sys.path.append('../python')
from slownacl.curve25519 import smult_curve25519, smult_curve25519_base

hexdigits = '0123456789abcdefABCDEF'

b32chars = 'abcdefghijklmnopqrstuvwxyz234567'

b32values = {'a':0, 'b':1, 'c':2, 'd':3, 'e':4, 'f':5, 'g':6, 'h':7,
'i':8, 'j':9, 'k':10, 'l':11, 'm':12, 'n':13, 'o':14, 'p':15, 'q':16,
'r':17, 's':18, 't':19, 'u':20, 'v':21, 'w':22, 'x':23, 'y':24, 'z':25,
'2':26, '3':27, '4':28, '5':29, '6':30, '7':31 }

def getPublic():
    secret = privatekey.get()
    publickey.delete(0, 'end')
    # base16?
    if len(secret) == 64 and isHex(secret):
        publickey.insert(0, binascii.hexlify(smult_curve25519_base(binascii.unhexlify(secret))))
    # base32?
    elif len(secret) == 51 and isb32(secret):
        publickey.insert(0, base32_encode(smult_curve25519_base(base32_decode(secret))))
    # try to pad it and use it anyway
    elif isHex(secret):
        privatekey.delete(0, 'end')
        privatekey.insert(0, secret.zfill(64))
        getPublic()
    # give up
    else:
        publickey.insert(0, 'Unable to decode Private key.')

def getShared():
    secret = privatekey.get()
    public = publickey.get()
    sharedkey.delete(0, 'end')
    if len(secret) == 64 and isHex(secret) and len(public) == 64 and isHex(public):
        sharedkey.insert(0, binascii.hexlify(smult_curve25519(binascii.unhexlify(secret), binascii.unhexlify(public))))
    elif len(secret) == 51 and isb32(secret) and len(public) == 51 and isb32(public):
        sharedkey.insert(0, base32_encode(smult_curve25519(base32_decode(secret), base32_decode(public))))
    else:
        sharedkey.insert(0, 'Unable to decode Private or Public keys.')

# https://stackoverflow.com/q/58437353
def isHex(s):
    return all(c in hexdigits for c in s)

def isb32(s):
    return all(c in b32chars for c in s)

def U8TO16_LE(x, i):
    return x[i] | (x[i+1]<<8)

def U16TO8_LE(p, pos, v):
    p.append((v     ) & 0xff)
    p.append((v >> 8) & 0xff)

def base32_encode(s):
    # Create a c255lbase32decode (Michele Bini) compatible base32 string.
    s = bytearray(s)
    n = []
    for i in range(0, len(s), 2):
        n.append(U8TO16_LE(s, i))
    r = ''
    for c in range(0, 255, 5):
        r = b32chars[c255lgetbin(n, c) + (c255lgetbin(n, c+1) << 1) + (c255lgetbin(n, c+2) << 2) + (c255lgetbin(n, c+3) << 3) + (c255lgetbin(n, c+4) << 4)] + r
    return r

def base32_decode(n):
    n = bytearray(n)
    r = [0] * 16
    l = len(n)
    for c in range(0, 255, 5):
        l -= 1
        v = b32values[chr(n[l])]
        c255lsetbit(r, c,    v&1)
        v = v >> 1
        c255lsetbit(r, c+1,  v&1)
        v = v >> 1
        c255lsetbit(r, c+2,  v&1)
        v = v >> 1
        c255lsetbit(r, c+3,  v&1)
        v = v >> 1
        c255lsetbit(r, c+4,  v&1)
    o = []
    for i in range(0, 16):
        U16TO8_LE(o, i*2, r[i])
    return ''.join(map(chr, o))

def c255lgetbin(n, c):
    return (n[c >> 4] >> (c & 0xf)) & 1

def c255lsetbit(n , c, v):
    i = c >>4
    a = n[i]
    a = a + (1 << (c & 0xf)) * v
    n[i] = a

top = tk.Tk()
top.title('X25519')
top.geometry('700x100')
entryFont = Font(family='Consolas', size=10)

privatekeylabel = tk.Label(top, text='Private key:')
privatekeylabel.grid(row=0, column=0)

privatekey = tk.Entry(top, font=entryFont, width=65)
privatekey.grid(row=0, column=1)

privatekey.insert(0, 'a0e7cb1a31ef588cf7c0a7d537a1a02d72d47667a7964f1d8e35f904e95f3d65')

genpub = tk.Button(top, text='Generate Public', width=14, command=getPublic)
genpub.grid(row=1, column=0)

publickey = tk.Entry(top, font=entryFont, width=65)
publickey.grid(row=1, column=1)

genshared = tk.Button(top, text='Generate Shared', width=14, command=getShared)
genshared.grid(row=2, column=0)

sharedkey = tk.Entry(top, font=entryFont, width=65)
sharedkey.grid(row=2, column=1)

top.mainloop()
