#!/usr/bin/env python

import sys
import binascii

if sys.version_info.major == 2:
    # We are using Python 2.x
    lt_3 = True
    import Tkinter as tk
    from tkFont import Font
elif sys.version_info.major == 3:
    # We are using Python 3.x
    lt_3 = False
    import tkinter as tk
    from tkinter.font import Font

sys.path.append('../python')
from pure25519.eddsa import signature, verify, publickey

def getPublic():
    secret = binascii.unhexlify(privatekey.get())
    pubkey.delete(0, 'end')
    pubkey.insert(0, binascii.hexlify(publickey(secret)))

def getSig():
    secret = binascii.unhexlify(privatekey.get())
    pk = binascii.unhexlify(pubkey.get())
    msg = binascii.unhexlify(message.get())
    r.delete(0, 'end')
    s.delete(0, 'end')

    sighex = binascii.hexlify(signature(msg, secret, pk))
    r.insert(0, sighex[0:64])
    s.insert(0, sighex[64:])

def verifySig():
    pk = binascii.unhexlify(pubkey.get())
    msg = binascii.unhexlify(message.get())
    sig = binascii.unhexlify(r.get() + s.get())
    v.delete(0, 'end')

    try:
        if verify(pk, sig, msg):
            v.insert(0, 'Verified')
        else:
            v.insert(0, 'Failed to verify')
    except:
        v.insert(0, 'Failed to verify')

top = tk.Tk()
top.title('Ed25519')
#top.geometry('710x610')
top.geometry('700x200')
entryFont = Font(family='Consolas', size=10)

privatekeylabel = tk.Label(top, text='Private key:')
privatekeylabel.grid(row=0, column=0)

privatekey = tk.Entry(top, font=entryFont, width=65)
privatekey.grid(row=0, column=1)

privatekey.insert(0, '833fe62409237b9d62ec77587520911e9a759cec1d19755b7da901b96dca3d42')

genpub = tk.Button(top, text='Make Public Key', width=14, command=getPublic)
genpub.grid(row=1, column=0)

pubkey = tk.Entry(top, font=entryFont, width=65)
pubkey.grid(row=1, column=1)

messagelabel = tk.Label(top, text='Message')
messagelabel.grid(row=2, column=0)

message = tk.Entry(top, font=entryFont, width=65)
message.grid(row=2, column=1)

message.insert(0, 'ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f')

rlabel = tk.Label(top, text='R')
rlabel.grid(row=3, column=0)

r = tk.Entry(top, font=entryFont, width=65)
r.grid(row=3, column=1)

slabel = tk.Label(top, text='S')
slabel.grid(row=4, column=0)

s = tk.Entry(top, font=entryFont, width=65)
s.grid(row=4, column=1)

gensig = tk.Button(top, text='Generate Signature', width=14, command=getSig)
gensig.grid(row=5, column=0)

#sig = tk.Entry(top, font=entryFont, width=65)
#sig.grid(row=5, column=1)

vsig = tk.Button(top, text='Verify Signature', width=14, command=verifySig)
vsig.grid(row=6, column=0)

v = tk.Entry(top, font=entryFont, width=65)
v.grid(row=6, column=1)

#textarea = tk.Text(top, font=entryFont)
#textarea.grid(row=7, column=0, columnspan=2, sticky='nsew')

#yscrollbar=tk.Scrollbar(top, orient='vertical', command=textarea.yview)
#yscrollbar.grid(row=7, column=2, pady=0, sticky='nse')
#textarea['yscrollcommand']=yscrollbar.set

top.mainloop()
