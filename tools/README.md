# Tools
### Some stand-alone tools I wrote for testing
* favicon.txt - List of commands used to generate the favicon.ico file.
* keygen.hta - Given a private key this tool will generate the X25519 compatible public key.  Given a private and public key, it will generate the shared secret.
* managekeys.hta - Manage mrblue.ini without running mrblue.hta.  This was a mockup.  Generating signing keys does not work, but adding public keys does.  Just run mrblue.hta to manage your keys.
* sign.hta - Ed25519 test code.  It can generate a public key from a private key.  It can sign a message and verify a signature.
