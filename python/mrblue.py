#!/usr/bin/env python

defaulthost = 'http://bluebanter.com/mrblue/default'

# debug level.
# Set to 1 to see all traffic to and from the relay (other than polling).
# Set to 2 to add key generation information.
# Set to 5 to add the secret keys.
# Set to 6 to see all polling.
debug = 0

#               info,   you,    them,   relay,  debug
msgcolor     = ('#cca', '#ccf', '#ffc', '#ada', '#fff')
# selected text colors
smsgcolor    = ('#442', '#006', '#660', '#141', '#000')

from sys import version_info
if version_info.major == 2:
    # We are using Python 2.x
    lt_3 = True
    import Tkinter as tk
    from tkFont import Font
elif version_info.major == 3:
    # We are using Python 3.x
    lt_3 = False
    import tkinter as tk
    from tkinter.font import Font

import os
import time
import base64
import hashlib
import binascii
import requests
import threading
import collections
import configparser

from pure25519.eddsa import signature, verify, publickey
from chacha20poly1305 import ChaCha20Poly1305
from slownacl.curve25519 import smult_curve25519, smult_curve25519_base
from VerticalScrolledFrame import VerticalScrolledFrame

# Verify SSL certificates
try:
    import urllib3.contrib.pyopenssl
    urllib3.contrib.pyopenssl.inject_into_urllib3()
except ImportError:
    pass

class MrBlue(tk.Tk):

    def __init__(self):
        tk.Tk.__init__(self)
        self.title('Mr. Blue')
        self.geometry('820x540')
        self.configure(bg='#000')
        self.protocol('WM_DELETE_WINDOW', self.real_window_closing)
        iconstring = b'iVBORw0KGgoAAAANSUhEUgAAAQAAAAEABAMAAACuXLVVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAG1BMVEUAAAAzM/8zM/8AAAAAAAAAAAAzM/8AAAD///+dRvNcAAAABnRSTlMAWxBbEB/WX3AhAAAAAWJLR0QIht6VegAAAAd0SU1FB+YEFA4FJ/TRWBMAAAHmSURBVHja7ZrLbRtBEAUJMAGHYCiJufLgRKRDRyCB6fsjS3fOe0DtElUJsFAgd2e6ebmIiIiIiIiIiIiIiIiUeaUF5gctQCcYOsHQCYZOMHSCoRMMnWDoBEMnGDrB0AmGTjB0gmknmE1qCXYFagm2BVoJtgVaCfYFSgn2BUoJAoFOgkCgkyARqCRIBCoJIoFGgkigkSATKCTIBAoJQoE8QSiQJ0gF3mgBugD+HYgDnP05kAc4+bugEODc54FGgFOfCSsBznwv6AQ48d2wFICfD7w8SDvAbjF8RoQFOMqckAtwkFkxGOAY+wIywCF2RmiAI+wN2QAH2B3DAfj/D/ykBURERERERESenus7LLBggesdFliwwPUOCyxY4E8AVmDBAn8DoAILFvgXgBRYsMBnAFBgwQL/A3ACCxb4CvAwLeEFC2wHaAlsBygJ7AcoCewH6AgEAToCQYCKQBKgIpAEaAhEARoCUYCCQBagIJAFyAXCALlAGCAXCD//CQrg3wH8V8A/B/AnIf8uwN+G/HkAPxHxZ0L8VMzfC/CbEX83xG/H/HzgO8HHr8e4tQTwGRE+JePnhPiklJ8V49Nyfl+Ab0z4nRG+NeP3hvjmlN8d49tz/v8DlxstICIiIiIiIiIiIiLydPwGISpXfQae1S0AAAAldEVYdGRhdGU6Y3JlYXRlADIwMjItMDQtMjBUMTg6MDU6MzktMDQ6MDB8kf1yAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIyLTA0LTIwVDE4OjA1OjM5LTA0OjAwDcxFzgAAAABJRU5ErkJggg=='
        icon = tk.PhotoImage(data=iconstring)
        self.tk.call('wm', 'iconphoto', self._w, icon)

        global debug, msgcolor
        self.aad          = bytearray.fromhex('073960b5276e319d1a1ccc71')
        self.signingaad   = bytearray.fromhex('653e68f3385980ac5db723a8')
        self.mefirst      = 0
        self.secret       = None
        self.shared_key   = None
        self.inifile      = 'mrblue.ini'
        self.iniversion   = 2
        self.signingkey   = None
        self.rekeying     = False
        self.oldkeys      = []
        self.pubcounter   = 0
        self.privcunter   = 0
        self.pubkeys      = collections.OrderedDict()
        self.privkeys     = collections.OrderedDict()
        self.queue        = []
        self.connected    = 0
        self.running      = True
        self.s            = requests.Session()
        self.debug        = debug
        self.msgcolor     = msgcolor
        self.privrow      = 1
        self.pubrow       = 2
        self.sethost      = False
        self.passok       = tk.IntVar()
        self.changeok     = tk.IntVar()
        self.polling      = None
        self.pollingQueue = None

        self.showGUI()
        self.createkeymanager()
        self.readINI()
        self.createaskpass()
        self.createchangepass()

    def WriteData(self, data, color=0, enc=False):
        if enc:
            sp = '_'
        else:
            sp = ' '
        timestamp = time.strftime('%I:%M:%S')
        self.textarea.configure(state='normal')
        try:
            self.textarea.insert('end', '\n' + timestamp + sp + data, color)
        except tk.TclError:
            self.textarea.insert('end', '\n' + timestamp + sp + data.encode('utf-8'), color)
        self.textarea.configure(state='disabled')
        # scroll down
        self.textarea.yview_moveto(1)

    def killpolling(self):
        if isinstance(self.polling, threading._Timer) and self.polling.isAlive():
            self.polling.cancel()
            self.polling.join()
        if isinstance(self.pollingQueue, threading._Timer) and self.pollingQueue.isAlive():
            self.pollingQueue.cancel()
            self.pollingQueue.join()

    def real_window_closing(self):
        self.running = False
        self.killpolling()
        self.window_closing()
        self.destroy()

    def window_closing(self):
        if self.shared_key != None:
            self.shared_key = [0] * len(self.shared_key)
            self.shared_key = None
        self.clearoldkeys()
        self.nokey()
        if self.secret != None:
            self.secret = [0] * len(self.secret)
            self.secret = None
        if self.connected == 2:
            data = {'disconnected': '1'}
            self.s.post(self.myhost, data=data)

    def clearoldkeys(self):
        if self.oldkeys != None and len(self.oldkeys) > 1:
            for i in range(len(self.oldkeys)):
                if type(self.oldkeys[i]).__name__ == 'int':
                    self.oldkeys[i] = 0
                else:
                    self.oldkeys[i] = [0] * len(self.oldkeys[i])
        self.oldkeys = []

    def connect_onClick(self):
        if not self.connected:
            self.signingkeyoption.configure(state='disabled')
            self.WriteData('Connecting...')
            self.myhost = self.host.get()
            self.mycounter = 0
            self.theircounter = 0
            self.theirname = 'them'
            self.running = True
            keys64 = self.genkey()
            self.connected = 1
            data = {'key': keys64}
            self.s.post(self.myhost, data=data, hooks = {'response': self.getdata_callback})
            if self.debug:
                self.WriteData('DEBUG1: <to relay>   key=' + keys64, 4)
        else:
            self.disconnect()

    def disconnect(self):
        self.running = False
        self.killpolling()
#        XHR.abort()
        self.WriteData('Disconnected')
        self.send.configure(state='disabled', background='#888')
        self.connect.configure(text='Connect')
        self.signingkeyoption.configure(state='normal')
        self.clicked.set('Select Key')
        self.window_closing()
        self.connected = 0
        self.mefirst = 0

    def getdata(self):
        data = {'p': ''}
        self.s.post(self.myhost, data=data, hooks = {'response': self.getdata_callback})
        if self.debug >= 6:
            self.WriteData('DEBUG6: <to relay>   p=', 4)

    def getdata_callback(self, response, *args, **kwargs):
        if response.status_code != requests.codes.ok:
            self.WriteData('Error connecting: ' + response.status_code)
            self.disconnect()
        elif self.connected and response.text != '':
            self.handle_response(response.text.splitlines())
        if self.connected == 2 and self.running:
            self.polling = threading.Timer(2, self.getdata)
            self.polling.start()

    def handle_response(self, res):
        done = False
        for i in res:
            if i != '':
                if self.debug:
                    self.WriteData('DEBUG1: <from relay> ' + i, 4)
                if i[:4] == 'key=':
                    self.handle_key(base64_decode(i[4:]))
                elif i[0] == '!':
                    if i[1] == '0':
                        self.WriteData('<relay> Other client disconnected.', 3)
                        self.connected = 0
                        done = True
                        self.disconnect()
                    elif i[1] == '1':
                        self.WriteData('<relay> Room created.  Waiting on second user.', 3)
                        self.mefirst = 1
                        self.relayConnected()
                    else:
                        self.WriteData('<relay> ' + i[1:], 3)
                    if done:
                        break
                else:
                    encryptedBytes = bytearray(base64_decode(i))
                    decryptedBytes = self.ccp.open(self.theirnonce, encryptedBytes, self.theiraad, self.theircounter)
                    self.theircounter += 2 + (len(encryptedBytes)-16)//64
                    if decryptedBytes == None:
                        self.WriteData('Encryption failed.  MAC does not match.')
                    else:
                        printme = str(decryptedBytes)
                        if printme[:5] == '!key=':
                            oldmyname = self.myname
                            if not self.rekeying:
                                self.pollingQueue.cancel()
                                self.rekey.configure(state='disabled')
                                self.send.configure(state='disabled', background='#888')
                                self.WriteData('Rekey requested.')
                                self.oldkeys = [self.mynonce, self.myaad, self.mycounter]
                                self.rekeying = True
                                keys64 = self.genkey()
                                self.queue.append('!key=' + keys64)
                                self.senddata()
                            self.clearoldkeys()
                            self.handle_key(base64_decode(printme[5:]))
                            self.myname = oldmyname
                            self.rekeying = False
                        else:
                            if printme[:2] in ('\\\\', '\\!'):
                                printme = printme[1:]
                            self.WriteData('<' + self.theirname + '> ' + printme, 2, True)

    def relayConnected(self):
        self.killpolling()
        self.polling = threading.Timer(0.5, self.getdata)
        self.polling.start()
        self.pollingQueue = threading.Timer(1, self.senddata)
        self.pollingQueue.start()
        self.connect['text'] = 'Disconnect'
        self.chat.focus_set()
        self.connected = 2

    def senddata(self):
        if len(self.queue) > 0:
            data = ''
            for i in self.queue:
                if self.rekeying:
                    # Use old key until new key is calculated.
                    encryptedBytes = self.ccp.seal(self.oldkeys[0], bytearray(i, 'utf8'), self.oldkeys[1], self.oldkeys[2])
                    self.oldkeys[2] += 2 + (len(encryptedBytes)-16)//64
                else:
                    encryptedBytes = self.ccp.seal(self.mynonce, bytearray(i, 'utf8'), self.myaad, self.mycounter)
                    self.mycounter += 2 + (len(encryptedBytes)-16)//64
                data += '&m[]=' + base64_encode(encryptedBytes)
            self.queue = []
            self.s.post(self.myhost, data=data[1:], hooks = {'response': self.senddata_callback}, headers={'Content-Type': 'application/x-www-form-urlencoded'})
            if self.debug:
                self.WriteData('DEBUG1: <to relay>   ' + data[1:], 4)
        elif self.running:
            self.pollingQueue = threading.Timer(1, self.senddata)
            self.pollingQueue.start()

    def senddata_callback(self, response, *args, **kwargs):
        if response.status_code != requests.codes.ok:
            self.WriteData('Error sending: ' + response.status_code)
            self.disconnect()
        elif self.running:
            self.pollingQueue = threading.Timer(1, self.senddata)
            self.pollingQueue.start()

    def send_onClick(self, event=None):
        if str(self.send['state']) == 'disabled':
            return
        if self.chat.get() != '':
            sendme = self.chat.get()
            self.WriteData('<' + self.myname + '> ' + sendme, 1, True)
            if sendme[0] in ('\\', '!'):
                sendme = '\\' + sendme
                if self.debug:
                    self.WriteData('DEBUG1: Prepending escape character.', 4)
            self.queue.append(sendme)
            self.chat.delete(0, 'end')
            self.chat.focus_set()

    def handle_key(self, keys):
        self.theiraad = bytearray(self.aad)
        # Signed key
        if len(keys) == 140:
            pk = keys[:32]
            if self.debug:
                self.WriteData('DEBUG1: Found signing key ' + base64_encode(pk), 4)
            self.theiraad[-1] += 1
            if self.debug >= 2:
                self.WriteData('DEBUG2: Changing their aad to ' + padlist(self.theiraad, 3), 4)
            s = keys[32:96]
            keys = keys[96:]
            if verify(pk, s, keys):
                self.WriteData('Valid signature')
            else:
                self.WriteData('Invalid signature')
                self.connected = 2
                self.disconnect()
                return
            b64pk = base64_encode(pk)
            if b64pk in self.pubkeys:
                self.theirname = self.pubkeys[b64pk]
                self.WriteData('Setting their name to ' + self.theirname + '.')
            else:
                self.WriteData('Unknown signing key: ' + b64pk)
        if len(keys) != 44:
            self.WriteData('Unknown key format.')
            self.disconnect()
            return
        self.theirnonce = bytearray(keys[:12])
        # Force some distance between nonces.
        if debug >= 2:
            self.WriteData('DEBUG2: My Nonce:            ' + padlist(self.mynonce, 3), 4)
            self.WriteData('DEBUG2: Their Nonce:         ' + padlist(self.theirnonce, 3), 4)
        if self.mefirst:
            self.mynonce[8]    |= 0x1
            self.theirnonce[8] &= 0xfe
        else:
            self.mynonce[8]    &= 0xfe
            self.theirnonce[8] |= 0x1
        if self.debug >= 2:
            self.WriteData('DEBUG2: Updated My Nonce:    ' + padlist(self.mynonce, 3), 4)
            self.WriteData('DEBUG2: Updated Their Nonce: ' + padlist(self.theirnonce, 3), 4)
        self.theirpublic = keys[12:]
        theirpublic32 = base32_encode(self.theirpublic)
        self.WriteData('Their Public Key: ' + theirpublic32)
        self.shared_key = smult_curve25519(self.secret, self.theirpublic)
        self.secret = [0] * len(self.secret)
        self.secret = None
        h = hashlib.new('blake2s256')
        h.update(self.shared_key)
        if self.debug >= 5:
            self.WriteData('DEBUG5: Shared key: ' + padlist(self.shared_key, 3), 4)
            self.WriteData('DEBUG5: Hashed key: ' + padlist(h.digest(), 3), 4)
        self.shared_key = h.digest()
        self.ccp = ChaCha20Poly1305(self.shared_key)
        self.shared_key = '0' * len(self.shared_key)
        self.shared_key = None
        h.update(os.urandom(32))
        h = None
        self.mycounter = 0
        self.theircounter = 0
        self.send.configure(state='normal', bg='#009', fg='#cca')
        self.rekey.configure(state='normal')
        self.WriteData('Encryption Ready')
        self.relayConnected()

    def keychange(self, name, key):
        self.clicked.set(name)
        if key != '':
            self.signingkey = decryptSigningKey('', self.signingaad, self.privkeys[key][1])
            if self.signingkey == None:
                self.passok.set(0)
                self.showaskpass()
                self.askpasspass.focus_set()
                self.askpassok.wait_variable(self.passok)
                if self.passok.get() != 1:
                    return
                self.hideaskpass()
                self.trypass(self.askpasspass.get(), key)
                self.askpasspass.delete(0, 'end')
            else:
                self.mykey = key
                self.WriteData('Using signing key: ' + key)
        else:
            self.signingkey = None

    def trypass(self, password, key):
        self.signingkey = decryptSigningKey(password, self.signingaad, self.privkeys[key][1])
        if self.signingkey == None:
            self.clicked.set('Select Key')
            self.WriteData('Failed to decrypt key')
        else:
            self.mykey = key
            self.WriteData('Using signing key: ' + key)

    def nokey(self, event=None):
        if self.signingkey != None:
            self.signingkey = [0] * len(self.signingkey)
            self.signingkey = None

    def rekeyConnection(self):
        if self.rekeying:
            return
        if self.connected == 2:
            oldmyname = self.myname
            self.WriteData('Rekey Initiated.')
            self.rekey.configure(state='disabled')
            self.send.configure(state='disabled', background='#888')
            self.rekeying = True
            self.oldkeys = [self.mynonce, self.myaad, self.mycounter]
            keys64 = self.genkey()
            self.queue.append('!key=' + keys64)
            self.myname = oldmyname
        else:
            self.WriteData('Not connected')

    def genkey(self):
        self.mynonce = bytearray(os.urandom(12))
        if self.debug >= 2:
            self.WriteData('DEBUG2: My Nonce:            ' + padlist(self.mynonce, 3), 4)
        self.secret = os.urandom(32)
        if self.debug >= 5:
            self.WriteData('DEBUG5: Secret key:          ' + padlist(self.secret, 3), 4)
        public_key = smult_curve25519_base(self.secret)
        public_key32 = base32_encode(public_key)
        keys8 = bytearray(self.mynonce)
        keys8.extend(public_key)
        self.myaad = bytearray(self.aad)
        if self.signingkey != None:
            self.myname = self.clicked.get()
            self.WriteData('Setting your name to ' + self.myname + '.')
            self.myaad[-1] += 1
            if debug >= 2:
                self.WriteData('DEBUG2: Changing my aad to   ' + padlist(self.myaad, 3), 4)
            pk = bytearray(base64_decode(self.mykey))
            sig = signature(keys8, self.signingkey, pk)
            self.clicked.set('Select Key')
            self.nokey()
            pk.extend(sig)
            pk.extend(keys8)
            keys64 = base64_encode(pk)
        else:
            self.myname = 'me'
            keys64 = base64_encode(keys8)
        self.WriteData('Your Public Key:  ' + public_key32)
        return keys64

    def readINI(self):
        config = configparser.ConfigParser()
        try:
            config.read(self.inifile)
        except:
            return
        if config.has_section('Public'):
            for name, key in config.items('Public'):
                self.addpublic(name.strip(), key.strip())
        if config.has_section('Private'):
            for name, key in config.items('Private'):
                self.addprivate(name.strip(), *reversed(key.strip().split(',')))
        if config.has_section('MrBlue'):
            if config.has_option('MrBlue', 'host'):
                confighost = config.get('MrBlue', 'host').strip()
                if confighost != '':
                    self.host.delete(0, 'end')
                    self.host.insert(tk.END, confighost)
                    self.confighost = confighost
                    self.sethost = True

    def writeINI(self):
        try:
            cfgfile = open(self.inifile, 'w')
        except:
            WriteData('Unable to save config file: ' + self.inifile)
            return
        config = configparser.ConfigParser()
        config.add_section('Private')
        # index optionmenu
        keyindex = {}
        for i in range(self.signingkeyoption['menu'].index('end')):
            keyindex[self.signingkeyoption['menu'].entrycget(i, 'label')] = i
        for key in self.privkeys:
            oldname = self.privkeys[key][0]
            newname = self.kmprivatekeysframe.nametowidget('priv1' + key).get().strip()
            if oldname != newname:
                self.privkeys[key][0] = newname
                self.signingkeyoption['menu'].entryconfigure(keyindex[oldname], label=newname)
            config.set('Private', self.privkeys[key][0], self.privkeys[key][1] + ',' + key)
        config.add_section('Public')
        for key in self.pubkeys:
            self.pubkeys[key] = self.kmpublickeysframe.nametowidget('pub1' + key).get()
            config.set('Public', self.pubkeys[key], key)
        config.add_section('MrBlue')
        config.set('MrBlue', 'iniversion', str(self.iniversion))
        if self.sethost:
            config.set('MrBlue', 'host', self.confighost)
        config.write(cfgfile)
        cfgfile.close()
        self.save.configure(background='#000')

    def generatekey(self):
        displayname = self.kmgenkeydisplayname.get()
        password = self.kmgenkeypassword.get()
        self.kmgenkeydisplayname.delete(0, 'end')
        self.kmgenkeypassword.delete(0, 'end')
        if displayname == '':
            displayname = 'default'
        sk = os.urandom(32)
        pk = publickey(sk)
        self.addprivate(displayname, base64_encode(pk), encryptSigningKey(password, self.signingaad, sk))
        self.highlightsave()

    def changepasswrapper(self, key):
        self.changeok.set(0)
        self.showchangepass()
        self.changepassold.focus_set()
        self.changepassok.wait_variable(self.changeok)
        if self.changeok.get() != 1:
            return
        self.hidechangepass()
        self.changepass(key)

    def changepass(self, key):
        old  = self.changepassold.get()
        new1 = self.changepassnew1.get()
        new2 = self.changepassnew2.get()
        if new1 != new2:
            self.WriteData('New Passwords do not match.')
        else:
            skey = decryptSigningKey(old, self.signingaad, self.privkeys[key][1])
            if skey == None:
                self.WriteData('Old Password is incorrect.')
            else:
                newskey = encryptSigningKey(new1, self.signingaad, skey)
                self.privkeys[key][1] = newskey
                self.highlightsave();
                self.WriteData('Password changed.')
        self.changepassold.delete(0, 'end')
        self.changepassnew1.delete(0, 'end')
        self.changepassnew2.delete(0, 'end')

    def highlightsave(self, new=None):
        self.save.configure(background='#009')
        return True

    def addpubkey_onClick(self):
        self.addpublic(self.kmpublickeysnewname.get(), self.kmpublickeysnewkey.get())
        self.kmpublickeysnewname.delete(0, 'end')
        self.kmpublickeysnewkey.delete(0, 'end')
        self.highlightsave()

    def addpublic(self, name, key):
        self.pubkeys[key] = name
        displayname = tk.Entry(self.kmpublickeysframe, name='pub1' + key, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=12)
        displayname.insert(0, name)
        displaykey = tk.Label(self.kmpublickeysframe, name='pub2' + key, text=key, bg='#000', fg='#cca', font=self.kmFont)
        displayrem = tk.Button(self.kmpublickeysframe, name='pub3' + key, text='X', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=lambda :self.deletepublic(key))
        displayname.grid(row=self.pubrow, column=0, sticky='w', pady=4, padx=3)
        displaykey.grid(row=self.pubrow, column=1, sticky='w', pady=4, padx=3)
        displayrem.grid(row=self.pubrow, column=2, sticky='w', pady=4, padx=3)
        self.pubrow += 1
        # Highlight save if displayname is changed - uses validation callback
        vcmd = (displayname.register(self.highlightsave), '%P')
        displayname.config(validate = 'key', validatecommand = vcmd)

    def deletepublic(self, key):
        del self.pubkeys[key]
        for i in range(1, 4):
            self.kmpublickeysframe.nametowidget('pub' + str(i) + key).destroy()
        self.highlightsave()

    def addprivate(self, name, key, private):
        self.privkeys[key] = [name, private]
        self.signingkeyoption['menu'].add_command(label=name, command=lambda :self.keychange(name,key))
        displayname = tk.Entry(self.kmprivatekeysframe, name='priv1' + key, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=12)
        displayname.insert(0, name)
        displaykey = tk.Label(self.kmprivatekeysframe, name='priv2' + key, text=key, bg='#000', fg='#cca', font=self.kmFont)
        displayrem = tk.Button(self.kmprivatekeysframe, name='priv3' + key, text='X', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=lambda :self.deleteprivate(key))
        displaychange = tk.Button(self.kmprivatekeysframe, name='priv4' + key, text='Change', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=lambda :self.changepasswrapper(key))
        displayname.grid(row=self.privrow, column=0, sticky='w', pady=4, padx=3)
        displaykey.grid(row=self.privrow, column=1, sticky='w', pady=4, padx=3)
        displayrem.grid(row=self.privrow, column=2, sticky='w', pady=4, padx=3)
        displaychange.grid(row=self.privrow, column=3, sticky='w', pady=4, padx=3)
        self.privrow += 1
        # Highlight save if displayname is changed - uses validation callback
        vcmd = (displayname.register(self.highlightsave), '%P')
        displayname.config(validate = 'key', validatecommand = vcmd)

    def deleteprivate(self, key):
        keyname = self.privkeys[key][0]
        del self.privkeys[key]
        for i in range(1, 5):
            self.kmprivatekeysframe.nametowidget('priv' + str(i) + key).destroy()
        if self.clicked.get() == keyname:
            self.signingkeyoption['menu'].delete(self.signingkeyoption['menu'].index(self.clicked.get()))
            self.clicked.set('Select Key')
            self.nokey()
        else:
            for i in range(self.signingkeyoption['menu'].index('end')):
                if keyname == self.signingkeyoption['menu'].entrycget(i, 'label'):
                    self.signingkeyoption['menu'].delete(i)
                    break
        self.highlightsave()

    def showkeymanager(self):
        x = self.winfo_x() + self.winfo_width() - 700
        y = self.winfo_y()
        self.k.geometry('+' + str(x) + '+' + str(y))
        self.k.deiconify()

    def hidekeymanager(self):
        self.k.withdraw()

    def showchangepass(self):
        x = self.winfo_x() + self.winfo_width() / 2 - 150
        y = self.winfo_y() + self.winfo_height() / 2 - 100
        self.p.geometry('+' + str(x) + '+' + str(y))
        self.p.deiconify()

    def hidechangepass(self):
        self.changeok.set(2)
        self.p.withdraw()

    def showaskpass(self):
        x = self.winfo_x() + self.winfo_width() / 2 - 150
        y = self.winfo_y() + self.winfo_height() / 2 - 50
        self.ask.geometry('+' + str(x) + '+' + str(y))
        self.ask.deiconify()

    def hideaskpass(self):
        self.passok.set(2)
        self.ask.withdraw()

    def showkmpassword(self):
        if self.kmgenkeypassword.cget('show') == '':
            self.kmgenkeypassword.config(show='*')
            self.kmgenkeypasswordbutton.config(text='Show Password')
        else:
            self.kmgenkeypassword.config(show='')
            self.kmgenkeypasswordbutton.config(text='Hide Password')

    def showppassword(self):
        if self.changepassold.cget('show') == '':
            self.changepassold.config(show='*')
            self.changepassnew1.config(show='*')
            self.changepassnew2.config(show='*')
            self.changepassshowpass.config(text='Show Passwords')
        else:
            self.changepassold.config(show='')
            self.changepassnew1.config(show='')
            self.changepassnew2.config(show='')
            self.changepassshowpass.config(text='Hide Passwords')

    def showhelp(self):
        self.WriteData('');
        self.WriteData('Quick help:');
        self.WriteData('');
        self.WriteData('Keys:');
        self.WriteData('Click the "Keys" button to open the Key Manager.');
        self.WriteData('Type a Display Name and optionally a Password and click "Generate Signing Key".');
        self.WriteData('Your Display Name is displayed in the chat window when you type.  Your name is a good choice here.');
        self.WriteData('Send the public key to your chatting partner.');
        self.WriteData('Ask your chatting partner to send their public key to you.');
        self.WriteData('Add their name and public key under the "Known public keys" section and click "Add".');
        self.WriteData('This Display Name will display in the chat window when they type.  Their name is a good choice here.');
        self.WriteData('Close the Key Manager.');
        self.WriteData('');
        self.WriteData('Connecting:');
        self.WriteData('Click "Select Key" and select your private key.');
        self.WriteData('If you password protected the key you will be prompted for the password.');
        self.WriteData('You can have more than one private key.');
        self.WriteData('Type a unique URL in the "Relay URL" text input known only to you and your chatting partner.');
        self.WriteData('Click Connect.');
        self.WriteData('');
        self.WriteData('Chatting:');
        self.WriteData('The first to arrive will be told to wait.');
        self.WriteData('Once you are both connected you will both see "Encryption Ready".');
        self.WriteData('Type a message and press enter or click the "Send" button.');
        self.WriteData('The message will be encrypted with your shared key before being sent.');
        self.WriteData('Click "Disconnect" or close the window to end the chat session.');
        self.WriteData('');
        self.WriteData('For more information visit https://bitbucket.org/jzielke/mr-blue/')
        self.WriteData('');

    def showGUI(self):
        global defaulthost
        myFont = Font(family='Consolas', size=9)

        topframe = tk.Frame(self, bg='#000')
        topframe.pack(side=tk.TOP, fill='both')

        keysbutton = tk.Button(topframe, text='Keys', bg='#000', fg='#cca', command=self.showkeymanager, padx=2, pady=0, font=myFont)
        keysbutton.pack(side=tk.RIGHT, padx=10)

        self.clicked = tk.StringVar(self)
        self.clicked.set('Select Key')
        self.signingkeyoption = tk.OptionMenu(topframe, self.clicked, 'Select Key', command=self.nokey)
        self.signingkeyoption.configure(bg='#000', fg='#cca', activebackground='#000', activeforeground='#cca', padx=2, pady=0, width=9, bd=1, relief='flat', font=myFont)
        self.signingkeyoption['menu'].configure(bg='#000', fg='#cca')
        self.signingkeyoption.pack(side=tk.RIGHT, ipadx=4)

        helpbutton = tk.Button(topframe, text='Help', bg='#000', fg='#cca', command=self.showhelp, padx=2, pady=0, font=myFont)
        helpbutton.pack(side=tk.RIGHT, padx=15)

        relaylabel = tk.Label(topframe, text='Relay URL:', bg='#000', fg='#cca', font=myFont)
        relaylabel.pack(side=tk.LEFT)

        self.host = tk.Entry(topframe, width=50, bg='#000', fg='#cca', insertbackground='#cca', font=myFont)
        self.host.insert(tk.END, defaulthost)
        self.host.pack(side=tk.LEFT)

        self.connect = tk.Button(topframe, text='Connect', command=self.connect_onClick, bg='#000', fg='#cca', padx=2, pady=0, font=myFont)
        self.connect.pack(side=tk.LEFT, padx=2)

        self.rekey = tk.Button(topframe, text='Rekey', command=self.rekeyConnection, bg='#000', fg='#cca', padx=2, pady=0, font=myFont)
        self.rekey.pack(side=tk.LEFT, padx=(15, 2))

        self.textarea = tk.Text(self, wrap=tk.WORD, state='disabled', bg='#000', fg='#cca', font=myFont)
        self.textarea.pack(fill=tk.BOTH, expand=tk.YES)
        for i in range(0, len(msgcolor)):
            self.textarea.tag_config(i, foreground=msgcolor[i], selectforeground=smsgcolor[i])

        yscrollbar=tk.Scrollbar(self.textarea, orient='vertical', activebackground='#888', bg='#888', troughcolor='#000', command=self.textarea.yview)
        yscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.textarea['yscrollcommand']=yscrollbar.set

        bottomframe = tk.Frame(self, bg='#000')
        bottomframe.pack(side=tk.BOTTOM, fill=tk.BOTH, pady=5)

        self.send = tk.Button(bottomframe, text='Send', command=self.send_onClick, state='disabled', bg='#666', padx=2, pady=0, font=myFont)
        self.send.pack(side=tk.RIGHT, padx=5)

        self.chat = tk.Entry(bottomframe, bg='#000', fg='#cca', insertbackground='#cca', font=myFont)
        self.chat.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        self.chat.bind('<Return>', self.send_onClick)

    def createaskpass(self):
        self.ask = tk.Toplevel()
        self.ask.withdraw()
        self.ask.geometry('300x100')
        self.ask.wm_title('Mr. Blue Password')
        self.ask.configure(bg='#a00')
        self.ask.protocol('WM_DELETE_WINDOW', self.hideaskpass)

        self.askFont = Font(family='Consolas', size=14)

        askpasstitlelabel = tk.Label(self.ask, text='Password:', bg='#a00', fg='#cca', font=self.askFont)
        askpasstitlelabel.pack(anchor='center', pady=(15, 0))

        askpassentryframe = tk.Frame(self.ask, bg='#a00')
        askpassentryframe.pack()

        self.askpasspass = tk.Entry(askpassentryframe, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=22, show='*')
        self.askpasspass.pack(side=tk.LEFT)

        self.askpassok = tk.Button(askpassentryframe, text='Ok', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=lambda: self.passok.set(1))
        self.askpassok.pack(side=tk.RIGHT)

        self.ask.bind('<Return>', lambda event: self.passok.set(1))

    def createchangepass(self):
        self.p = tk.Toplevel()
        self.p.withdraw()
        self.p.geometry('300x200')
        self.p.wm_title('Mr. Blue Change Password')
        self.p.configure(bg='#a00')
        self.p.protocol('WM_DELETE_WINDOW', self.hidechangepass)

        self.pFont = Font(family='Consolas', size=14)

        changepasstitlelabel = tk.Label(self.p, text='Change Password', bg='#a00', fg='#cca', font=self.pFont)
        changepasstitlelabel.grid(row=0, column=1, pady=20)

        changepassoldlabel = tk.Label(self.p, text='Old:', bg='#a00', fg='#cca', font=self.pFont)
        changepassoldlabel.grid(row=1, column=0)

        self.changepassold = tk.Entry(self.p, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=22, show='*')
        self.changepassold.grid(row=1, column=1, pady=3)

        changepassnewlabel = tk.Label(self.p, text='New:', bg='#a00', fg='#cca', font=self.pFont)
        changepassnewlabel.grid(row=2, column=0)

        self.changepassnew1 = tk.Entry(self.p, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=22, show='*')
        self.changepassnew1.grid(row=2, column=1, pady=3)

        self.changepassnew2 = tk.Entry(self.p, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=22, show='*')
        self.changepassnew2.grid(row=3, column=1, pady=3)

        changepassbuttonframe = tk.Frame(self.p, bg='#a00')
        changepassbuttonframe.grid(row=4, column=1, sticky='ew', pady=3)

        self.changepassshowpass = tk.Button(changepassbuttonframe, text='Show Passwords', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=self.showppassword)
        self.changepassshowpass.pack(side=tk.LEFT)

        self.changepassok = tk.Button(changepassbuttonframe, text='Ok', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=lambda: self.changeok.set(1))
        self.changepassok.pack(side=tk.RIGHT)

        self.p.bind('<Return>', lambda event: self.changeok.set(1))

    def createkeymanager(self):
        self.k = tk.Toplevel()
        self.k.withdraw()
        self.k.geometry('700x400')
        self.k.wm_title('Mr. Blue Key Manager')
        self.k.configure(bg='#000')
        self.k.protocol('WM_DELETE_WINDOW', self.hidekeymanager)

        self.kmFont = Font(family='Courier New', size=10)
        self.kmFontBold = Font(family='Courier New', size=10, weight='bold')

        kmframe = VerticalScrolledFrame(self.k, bg='#000')
        kmframe.vsb.configure(activebackground='#888', bg='#888', troughcolor='#000')
        kmframe.pack(side='top', expand=True, fill='both')
        kmframe.bind('<Configure>', self.OnKMResize)

        self.kmtopframe = tk.Frame(kmframe, bg='#000')
        self.kmtopframe.pack(side=tk.TOP, expand=1, fill='x', pady=5)

        self.save = tk.Button(self.kmtopframe, text='Save', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=self.writeINI)
        self.save.pack(side=tk.RIGHT, padx=10, anchor='ne')

        kmgenkeyframe = tk.Frame(self.kmtopframe, bg='#000')
        kmgenkeyframe.pack(side=tk.TOP, anchor='nw')

        kmgenkeydisplaynamelabel = tk.Label(kmgenkeyframe, text='Display Name:', bg='#000', fg='#ccf', font=self.kmFont)
        kmgenkeydisplaynamelabel.grid(row=0, column=0, sticky='sw')

        kmgenkeynameframe = tk.Frame(kmgenkeyframe, bg='#000')
        kmgenkeynameframe.grid(row=0, column=1, sticky='w')

        self.kmgenkeydisplayname = tk.Entry(kmgenkeynameframe, bg='#000', fg='#ccf', insertbackground='#cca', font=self.kmFont, width=12)
        self.kmgenkeydisplayname.pack(side=tk.LEFT, pady=5)

        kmgensigningkeybutton = tk.Button(kmgenkeynameframe, text='Generate Signing Key', bg='#000', fg='#ccf', padx=2, pady=0, font=self.kmFont, command=self.generatekey)
        kmgensigningkeybutton.pack(side=tk.LEFT, padx=30)

        kmgenkeypasswordlabel = tk.Label(kmgenkeyframe, text='Password:', bg='#000', fg='#ccf', font=self.kmFont)
        kmgenkeypasswordlabel.grid(row=1, column=0, sticky='sw')

        kmgenkeypasswordframe = tk.Frame(kmgenkeyframe, bg='#000')
        kmgenkeypasswordframe.grid(row=1, column=1, sticky='sw')

        self.kmgenkeypassword = tk.Entry(kmgenkeypasswordframe, bg='#000', fg='#ccf', insertbackground='#cca', font=self.kmFont, width=37, show='*')
        self.kmgenkeypassword.pack(side=tk.LEFT)

        self.kmgenkeypasswordbutton = tk.Button(kmgenkeypasswordframe, text='Show Password', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=self.showkmpassword)
        self.kmgenkeypasswordbutton.pack(side=tk.LEFT, padx=5)

        self.kmtopframe.update()
        oldheight = self.kmtopframe.winfo_reqheight()
        self.kmtopframe.pack_propagate(0)
        self.kmtopframe.configure(height=oldheight, width=680)

        kmprivatekeyslabel = tk.Label(kmframe, text='My private keys:', bg='#000', fg='#cca', font=self.kmFontBold)
        kmprivatekeyslabel.pack(side=tk.TOP, anchor='nw')

        self.kmprivatekeysframe = tk.Frame(kmframe, bg='#000')
        self.kmprivatekeysframe.pack(side=tk.TOP, anchor='nw')

        kmprivatekeysnamelabel = tk.Label(self.kmprivatekeysframe, text='Display Name', bg='#000', fg='#cca', font=self.kmFont)
        kmprivatekeysnamelabel.grid(row=0, column=0, sticky='w')

        kmprivatekeyskeylabel = tk.Label(self.kmprivatekeysframe, text='Public Key', bg='#000', fg='#cca', font=self.kmFont)
        kmprivatekeyskeylabel.grid(row=0, column=1, sticky='w')

        kmprivatekeysremovelabel = tk.Label(self.kmprivatekeysframe, text='Remove', bg='#000', fg='#cca', font=self.kmFont)
        kmprivatekeysremovelabel.grid(row=0, column=2, sticky='w')

        kmprivatekeyspasswordlabel = tk.Label(self.kmprivatekeysframe, text='Password', bg='#000', fg='#cca', font=self.kmFont)
        kmprivatekeyspasswordlabel.grid(row=0, column=3, sticky='w')

        kmpublickeyslabel = tk.Label(kmframe, text='Known public keys:', bg='#000', fg='#cca', font=self.kmFontBold)
        kmpublickeyslabel.pack(side=tk.TOP, anchor='nw')

        self.kmpublickeysframe = tk.Frame(kmframe, bg='#000')
        self.kmpublickeysframe.pack(side=tk.TOP, anchor='nw')

        kmpublickeysnamelabel = tk.Label(self.kmpublickeysframe, text='Display Name', bg='#000', fg='#cca', font=self.kmFont)
        kmpublickeysnamelabel.grid(row=0, column=0, sticky='w')

        kmpublickeyskeylabel = tk.Label(self.kmpublickeysframe, text='Public Key', bg='#000', fg='#cca', font=self.kmFont)
        kmpublickeyskeylabel.grid(row=0, column=1, sticky='w')

        kmpublickeysaddremovelabel = tk.Label(self.kmpublickeysframe, text='Add/Remove', bg='#000', fg='#cca', font=self.kmFont)
        kmpublickeysaddremovelabel.grid(row=0, column=2, sticky='w')

        self.kmpublickeysnewname = tk.Entry(self.kmpublickeysframe, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=12)
        self.kmpublickeysnewname.grid(row=1, column=0, sticky='w', pady=4, padx=3)

        self.kmpublickeysnewkey = tk.Entry(self.kmpublickeysframe, bg='#000', fg='#cca', insertbackground='#cca', font=self.kmFont, width=43)
        self.kmpublickeysnewkey.grid(row=1, column=1, sticky='w', pady=4, padx=3)

        kmpublickeysnewadd = tk.Button(self.kmpublickeysframe, text='Add', bg='#000', fg='#cca', padx=2, pady=0, font=self.kmFont, command=self.addpubkey_onClick)
        kmpublickeysnewadd.grid(row=1, column=2, sticky='w', pady=4, padx=3)

    def OnKMResize(self, event):
        self.kmtopframe.configure(width=event.width-20)

def encryptSigningKey(password, aad, sk):
    key   = bytearray(hashpass(password))
    nonce = os.urandom(12)
    skey  = ChaCha20Poly1305(key)
    enc   = skey.seal(nonce, bytearray(sk), aad, ord(nonce[3]))
    out   = bytearray(nonce)
    out.extend(enc)
    return base64_encode(out)

def decryptSigningKey(password, aad, enc64):
    key   = bytearray(hashpass(password))
    enc   = base64_decode(enc64)
    nonce = enc[0:12]
    enc   = enc[12:]
    skey  = ChaCha20Poly1305(key)
    return skey.open(nonce, bytearray(enc), aad, ord(nonce[3]))

def hashpass(password):
    if password != '':
        h = hashlib.new('blake2s256')
        h.update(password)
        return h.digest()
    else:
        # Hard-code blank password, don't waste time calculating it.
        return bytearray.fromhex('69217a3079908094e11121d042354a7c1f55b6482ca1a51e1b250dfd1ed0eef9')

def padlist(l, n):
    hexstr = binascii.hexlify(l)
    out = ''
    for c in hexstr.decode('hex'):
        out += str(ord(c)).zfill(n) + ','
    return out [:-1]

def U8TO16_LE(x, i):
    return x[i] | (x[i+1]<<8)

def base64_encode(string):
    return base64.urlsafe_b64encode(str(string)).rstrip(b'=')

def base64_decode(string):
    padding = 4 - (len(string) % 4)
    return base64.urlsafe_b64decode(str(string) + ('=' * padding))

c255lbase32chars = 'abcdefghijklmnopqrstuvwxyz234567'

def base32_encode(string):
    # Create a c255lbase32decode (Michele Bini) compatible base32 string.
    global c255lbase32chars
    string = bytearray(string)
    n = []
    for i in range(0, len(string), 2):
        n.append(U8TO16_LE(string, i))
    r = ''
    for c in range(0, 255, 5):
        r = c255lbase32chars[c255lgetbin(n, c) + (c255lgetbin(n, c+1) << 1) + (c255lgetbin(n, c+2) << 2) + (c255lgetbin(n, c+3) << 3) + (c255lgetbin(n, c+4) << 4)] + r
    return r

def c255lgetbin(n, c):
    return (n[c >> 4] >> (c & 0xf)) & 1

if __name__ == '__main__':
    app = MrBlue()
    app.mainloop()
