# Mr. Blue
### A web-based message passing service.
## Usage
* Client 1 connects to this relay using a unique URL. Examples: http://domain/mrblue/something/unique or http://domain/mrblue/E7F300F122D5203C351C7B5646B97782/Ilovecats
* Client 2 connects to the same address.
* Anything sent by one client is received by the other.
## Clients
* mrblue.hta - HTML Application using [X25519](https://cr.yp.to/ecdh.html) (optionally signed with [Ed25519](https://ed25519.cr.yp.to/)) + [ChaCha20](https://cr.yp.to/chacha.html)-[Poly1305](https://cr.yp.to/mac.html).  The shared secret is hashed with [BLAKE2s](https://www.blake2.net/).
* mrblue.pyz - Python Zip Application compatible with the HTML Application above.
* The Mr. Blue web page - Type something random after the URL and you will get a JavaScipt client compatible with the applications above.
## Install
### Relay service
Only 2 files need to be in a directory served by the web server:

* htaccess.txt as .htaccess
* mrblue.php as index.php

*.htaccess* requires the [Apache](https://httpd.apache.org/) web server with the [mod\_rewrite](https://httpd.apache.org/docs/current/mod/mod_rewrite.html) module enabled. If you know how to write similar rules for another web server, feel free to do so.

*index.php* requires [PHP](https://www.php.net/). Two paths will need to be edited at the top.  **Both paths should be outside of the Apache web directories.**

* *$messagefolder* needs to point to a folder to store the messages being passed between clients.  The Apache server user (typically www-data) will need write access to this folder.
* *$htmlfolder* needs to point to a folder containing the files from the [/web/html/](https://bitbucket.org/jzielke/mr-blue/src/master/web/html/) folder.
* Optionally, *$hide* can be edited to disable the JavaScript client.
### Clients
* *JavaScript* - set *$hide* to 0 or 1
* *mrblue.hta* - If you want to make this available from your relay service, copy it to the folder *$htmlfolder* points to.
* *mrblue.pyz* - If you want to make this available you will need to build it, then copy it to the folder *$htmlfolder* points to.  To build from the MrBlue repo folder:

```sh
cd python
zip mrblue.pyz *.py */*.py requirements.txt
```

## Quickstart
Type in an address known to your chatting partner in the *Relay URL* field and click *Connect*.  Once *Encryption Ready* is displayed on the screen you can send encrypted messages.  Type in a message in the input box at the bottom and hit enter.  The message will be encrypted and sent to your chatting partner.

## Client Interface
*Relay URL* - The meeting address.  This should be known only to you and your chatting partner.

*Connect* - After the meeting address is entered click *Connect* to connect to the relay.  This will generate a random key, associated public key, and a random nonce.  The public key and nonce will be sent to the relay to be sent to your chatting partner.  The public key and nonce will optionally be signed with a signing key if one is selected.

*Disconnect* - The *Connect* button will become the *Disconnect* button after you are connected.  Click *Disconnect* to disconnect.

*Rekey* - This will calculate a new key, public key, and nonce to send to your chatting partner.  Their client will do the same when your new information is received.

*Help* - Displays a quick start guide.

*Select Key* - If you have generated signing keys they will appear in this dropdown select box.  If you set a password you will be prompted for it when you make the selection.

*Keys* - This will open the key manager.

**Mr. Blue Key Manager**

*Display Name* - When chatting with this signing key, this is the name that will show up for your client.

*Password* - Optionally password protect this new signing key.

*Show Password* - The password is hidden.  The *Show Password* button will reveal it.  Pressing it again will hide the password again.

*Generate Signing Key* - After the *Display Name* and optional *Password* fields are filled out the *Generate Signing Key* button will generate a signing key and its associated public key.

*Save / Export* - This will save the Public and Private keys.  In the JavaScript client, closing the key manager will save the keys to the web browser Local Storage, *Export* will download a config file with the keys.

*Import* - The JavaScript client has this button to load a saved config file into the web browser Local Storage.  The standalone clients automatically load mrblue.ini when they start.

*My private keys*

* *Display Name* - This can be edited.  After editing you need to click *Save* or *Export* to update your local copy.  Closing the key manager will save the changes to the web browser Local Storage in the JavaScript client.  Clicking *Export* is still necessary if you want to update your backup config file.
* *Remove* - The X on the line will remove this private key.
* *Password* - The *Change* button will let you change the password for the private key.
    * You will be prompted for the old password.  The *Show Passwords* button will unmask all 3 password fields.
    * The *Ok* button will try to decrypt the key with the old password and then encrypt the key with the new password.  If anything goes wrong in this process it will be logged in the *Scrollable Text Area*.
    * Remember to *Save* or *Export* your new config file.
    * If the private key was not password protected, leave the old password blank.
    * To remove the password from a password-protected key, leave both new password fields blank.

*Known public keys*

* *Display Name* - Type in a new name in the first row if adding a new public key.  The name can be changed later just like the private keys.  This is the name that will display in your client when chatting with someone that uses this signing key.
* *Public Key* - Paste the public key from your chat partner in the first row.
* *Add/Remove* - *Add* will add the new public key.  X will remove the saved public key.

**Remember to *Save* or *Export* after making changes in the key manager.**

*Scrollable Text Area* - This takes the largest amount of space in the client.  This is where all informational messages will be displayed as well as chat text.

*Chat input box* - This is under the *Scrollable Text Area*.  This is where you type your messages to send.  Press enter or click the Send button.

*Send* - This button is disabled until *Encryption Ready* appears in the *Scrollable Text Area*.  It is disabled again if the *Rekey* button is pressed.  It will stay disabled until *Encryption Ready* displays again.

## Putting it all together
Download a client or load the JavaScript client.  Open the Key Manager and create a private key.  Anyone can masquerade as you if they gain access to your computer if you do not password protect your private key.  The same thing is true if they get access to your backup config file.  Providing a password is highly recommended.

Send your public key to each chat partner you plan to use the signing key with.

Before connecting to your meeting room, select your signing key from the dropdown.  Enter your password if prompted.  Click *Connect*.  If your chatting partner is already there you will be ready to chat.  If not, the relay will inform you that you are the first to arrive.

If you select a signing key a message showing the associated public key will show up in the *Scrollable Text Area*.  After clicking *Connect* a message will show that your chat name is being changed.  This is for your display only.

Your public key will show up next.  If you do not sign your key, you can use the displayed public keys to detect a Man-in-the-Middle attack by discussing them in plain text.

If your chat partner signed their public key, you should see 'Valid signature'.  If this is a known public key, their chat name will be set from the associated name in your key manager.

After that, you should see 'Encryption Ready' and you are ready to chat.

Since you do not have a shared key when you start, the public keys are sent in the clear.  You can connect to an https version of the relay, if available, if you want that extra layer of protection.

If you click *Rekey* the new keys will be negotiated using your old shared key - meaning the new set will be encrypted and not sent in the clear like the original public keys were.

If you do not use signed keys the chat names will be generic (me/them) to remind you that the identities are not authenticated.

## Why?
There are much better options for instant messaging with forward secrecy, automatic public key transfers, always connected, instant notifications, mobile apps, etc.

While watching a movie I saw two of the characters communicating via an encrypted chat.  I wondered how I would do that if I wanted.  This is my answer.  I wanted the client to be written in a scripted language so that it could be read and audited.  You can read and modify the client with a text editor.  You do not need any development tools to run the code.  You do not need an installer.  In a pinch you can run it in your web browser.

The relay code is simple and you don't have to trust it as long as you trust the client.

## Protocol
* On first connection, POST a variable named 'key'.
 * The first client will receive '!1' to signify they are alone.
 * The second client will receive 'key=' followed by the key POSTed by the first client.
 * The first client will receive 'key=' followed by the key POSTed by the second client.
 * The meeting address no longer exists. If a third client connects they will be treated as the first client. They can coexist without interference.
* Messages to be relayed are sent via a POST variable named 'm[]'. Multiple messages can be sent in one POST. They will be received in the same order they were sent.
* Periodically (500ms) poll for messages. Messages are separated by a newline '\n' character.
* POST 'disconnected=1' to end the session. This will delete any pending messages and inform the other client via '!0'.
* key and messages must be POSTed. Polling can use GET or POST but POST is preferred to prevent caching.
* This service relays exactly what was sent. It does not use the keys. The clients are responsible for securing their own messages.
