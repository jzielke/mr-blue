<?php

$messagefolder = '/home/web/mrblue/messages/';
$htmlfolder = '/home/web/mrblue/html/';

// hide = 0, here's who we are; this is what we do
// hide = 1, hide main page only, js client will still work
// hide = 2, only standalone clients will work
$hide = 0;

function hide_headers() {
  header_remove();
  ini_set('default_charset', '');
//  header('Server: nginx/1.2.1');
  header('Content-Type: text/html');
  header('Last-Modified: Mon, 04 Oct 2004 15:04:06 GMT');
//  header('Transfer-Encoding: chunked');
  header('Connection: keep-alive');
}

// Delete old (>60s) messages
$now = time();
foreach (glob($messagefolder.'*') as $file) {
  if (is_file($file) && $now - filemtime($file) > 60) unlink($file);
}

// No keys and no messages?  This is not a chat client.
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  header('Content-Encoding: gzip');
  ob_start("ob_gzhandler");
  if ($hide == 2) {
    hide_headers();
    $loadfile = 'nginx.html';
  } else {
    $dirlen = strlen(dirname($_SERVER['SCRIPT_NAME'])) + 1;
    // Allow cache breaking / versioning
    $vfile = strtok($_SERVER['REQUEST_URI'],'?');
    if (strlen($vfile) == $dirlen) {
      if ($hide) {
        hide_headers();
        $loadfile = 'nginx.html';
      } else {
        $loadfile = 'protocol.html';
      }
    } else {
      if (file_exists($htmlfolder.basename(substr($vfile, $dirlen)))) {
        $loadfile = basename(substr($vfile, $dirlen));
      } else {
        $loadfile = 'client.html';
      }
    }
  }
  if (substr($loadfile, -3) == '.js') {
    header('Content-Type: application/javascript');
  } elseif(substr($loadfile, -4) == '.css') {
    header('Content-Type: text/css');
  } elseif ($loadfile == 'mrblue.hta' || $loadfile == 'mrblue.pyz') {
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.$loadfile.'"');
  }

  // 304 Not Modified
  $last_modified_time = filemtime($htmlfolder.$loadfile);
  $etag = md5_file($htmlfolder.$loadfile);
  header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
  header("Etag: $etag");
  if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time || @trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) {
    header("HTTP/1.1 304 Not Modified");
    exit;
  }

  readfile($htmlfolder.$loadfile);
  ob_end_flush();
  exit;
}

session_set_cookie_params(['SameSite' => 'Strict']);
session_start();
header('Access-Control-Allow-Origin: *');

$room = hash('sha256', $_SERVER['REQUEST_URI']);
$mysid = session_id();

// Client hit connect button.
if (isset($_POST['key'])) {
  // We are the second person to arrive.
  if (file_exists($messagefolder.$room)) {
    // Send key and display message.
    list($_SESSION['theirsid'], $theirkey) = explode("\x00", file_get_contents($messagefolder.$room), 2);
    unlink($messagefolder.$room);
    file_put_contents($messagefolder.$room.'.key.'.$_SESSION['theirsid'], $mysid."\x00".$_POST['key']);
//    file_put_contents($messagefolder.'debug.txt', $_POST['key']."\n", FILE_APPEND);
    echo 'key='.$theirkey;
  } else {
    // Create room
    file_put_contents($messagefolder.$room, $mysid . "\x00" . $_POST['key']);
//    file_put_contents($messagefolder.'debug.txt', $_POST['key']."\n", FILE_APPEND);
    echo '!1';
  }
  exit;
// Client sent message(s).
} elseif (isset($_POST['m'])) {
  $ser = 1;
  $base = $messagefolder.$room.'.'.$_SESSION['theirsid'];
  $file = $base.'00000000';
  while (file_exists($file)) {
    $file = $base.(str_pad("$ser", 8, '0', STR_PAD_LEFT));
    $ser++;
  }
  foreach ($_POST['m'] as $m) {
    file_put_contents($file, $m."\n", FILE_APPEND);
  }
  exit;
} else if (isset($_POST['disconnected'])) {
  session_destroy();
  // Other client never connected.
  if (!isset($_SESSION['theirsid'])) {
    if (file_exists($messagefolder.$room)) {
      unlink($messagefolder.$room);
    }
  } else {
    // Erase stale messages.
    foreach (glob($messagefolder.$room.'.*{'.$_SESSION['theirsid'].','.$mysid.'}*', GLOB_BRACE) as $file) {
      unlink($file);
//      file_put_contents($messagefolder.'debug.txt', 'deleting '.$file."\n", FILE_APPEND);
    }
    // Disconnect the other client.
    file_put_contents($messagefolder.$room.'.'.$_SESSION['theirsid'], "!0");
  }
  exit;
}

if (file_exists($messagefolder.$room.'.key.'.$mysid)) {
  list($_SESSION['theirsid'], $theirkey) = explode("\x00", file_get_contents($messagefolder.$room.'.key.'.$mysid), 2);
  echo 'key=' . $theirkey;
  unlink($messagefolder.$room.'.key.'.$mysid);
  exit;
}

if (!isset($_SESSION['theirsid'])) {
  if (file_exists($messagefolder.$room)) {
    touch($messagefolder.$room);
  }
  sleep (2);
  exit;
}

$count = 0;
$countmax = 4;
while ($count < $countmax) {
  foreach (glob($messagefolder.$room.'.'.$mysid.'*') as $file) {
    $m = file_get_contents($file);
    echo $m;
    unlink($file);
    $count = $countmax;
    if ($m == '!0') {
      session_destroy();
//      file_put_contents($messagefolder.'debug.txt', 'disconnected by request '.$file."\n", FILE_APPEND);
    }
  }
  $count++;
  // Skip 1 second pause if message was sent
  if ($count > $countmax) {
    exit;
  }
  sleep(1);
}
?>
