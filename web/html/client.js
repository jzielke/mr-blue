var resizing, secret, shared_key, nonce, aad, myaad, theiraad, mycounter, theircounter, mynonce, theirnonce, mefirst, polling, pollingQueue, host, queue = [], connected, debug, rekeying, oldkeys;
var inifile, pubcounter = 0, privcounter = 0, pubkeys = {}, privkeys = {}, myname, theirname, signingaad, signingkey, iniversion, msgcolor;

// Debug level.
// Set to 1 to see all traffic to and from the server (other than polling).
// Set to 2 to add key generation information.
// Set to 5 to add the secret keys.
// Set to 6 to see all polling.
debug = 0;

//               info,   you,    them,   relay,  debug
msgcolor     = ["#cca", "#ccf", "#ffc", "#ada", "#fff"];

aad          = [0x07, 0x39, 0x60, 0xb5, 0x27, 0x6e, 0x31, 0x9d, 0x1a, 0x1c, 0xcc, 0x71];
signingaad   = [0x65, 0x3e, 0x68, 0xf3, 0x38, 0x59, 0x80, 0xac, 0x5d, 0xb7, 0x23, 0xa8];
mefirst      = 0;
shared_key   = [];
inifile      = "mrblue.ini";
iniversion   = 2;
signingkey   = false;
rekeying     = false;
oldkeys      = [];
defaulthost  = "";

$ = function(id) {
  return document.getElementById(id);
}

var xhttp = new XMLHttpRequest();
var httpQueue = new XMLHttpRequest();

var DOMtext = document.createTextNode("escapeHTML");
var DOMnative = document.createElement("span");
DOMnative.appendChild(DOMtext);

navigator.sendBeacon = navigator.sendBeacon || function (url, data) {
    xhttp.open("POST", url, false);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.send(data);
};

function init() {
  $("host").value = window.location.href;
  connected = false;
  resized();
  window.onresize = window_onResize;
  if ("orientation" in screen) {
    screen.orientation.onchange = window_onResize;
  }
  window.onbeforeunload = window_closing;
  $("connect").onclick = connect_onClick;
  $("send").onclick = send_onClick;
  $("addpubkey").onclick = addpubkey_onClick;
  $("save").onclick = writeINI;
  $("load").onclick = readINI;
  $("generatekey").onclick = generatekey;
  $("signingkey").onchange = keychange;
  $("askpass").onkeyup = passkeyup;
  $("askpassok").onclick = trypass;
  $("showpass").onclick = togglepass;
  $("showpasschange").onclick = togglechangepass;
  $("passchangeok").onclick = changepass;
  $("rekey").onclick = rekeyConnection;
  $("help").onclick = showHelp;
  loadJSON();
}

function window_onResize() {
  clearTimeout(resizing);
  resizing = setTimeout(resized, 100);
}

function resized() {
  $("htmltextarea").style.height = (window.innerHeight - $("htmltextarea").offsetTop - 50) + "px";
  $("chat").style.width = (document.body.clientWidth - $("send").offsetWidth - 24) + "px";

  // scale keymanager for mobile
  var keywidth  = keywidthscale  = 700 + 20;
  var keyheight = keyheightscale = 400 + 20;
  if (window.innerWidth < keywidth) {
    keywidthscale = window.innerWidth;
  }
  if (window.innerHeight < keyheight) {
    keyheightscale = window.innerHeight;
  }
  if (keywidth != keywidthscale || keyheight != keyheightscale) {
    var scaleX   = keywidthscale  / keywidth;
    var scaleY   = keyheightscale / keyheight;
    var scale    = Math.min(scaleX, scaleY, 1);
    $("keymanager").style.transform       = "scale(" + scale + ")";
    $("keymanager").style.transformOrigin = "right top";
    // If we are scaling the key manager, scale the password prompts as well
    $("passwordPrompt").style.transform   = "scale(" + scale + ")";
    $("changePass").style.transform       = "scale(" + scale + ")";
  } else {
    $("keymanager").style.transform     = "scale(1)";
    $("passwordPrompt").style.transform = "scale(1)";
    $("changePass").style.transform     = "scale(1)";
  }
}

function window_closing() {
  if (shared_key !== undefined) {
    for (var i=0; i<shared_key.length; i++) {
      shared_key[i] = 0;
    }
    shared_key = false;
  }
  clearoldkeys();
  if (secret !== undefined) {
    for (var i=0; i<secret.length; i++) {
      secret[i] = 0;
    }
    secret = false;
  }
  if (signingkey !== undefined) {
    for (var i=0; i<signingkey.length; i++) {
      signingkey[i] = 0;
    }
    signingkey = false;
  }
  if (connected == 2) {
    var params = new URLSearchParams({ disconnected: 1 });
    navigator.sendBeacon(host, params);
    if (debug) {
      WriteData("DEBUG1: <to relay>   disconnected=1", 4);
    }
  }
}

function scrolldown() {
  $("htmltextarea").scrollTop = $("htmltextarea").scrollHeight;
}

function connect_onClick() {
  if (!connected) {
    $("signingkey").disabled = true;
    WriteData("Connecting...");
    host = $("host").value;
    mycounter = 0;
    theircounter = 0;
    theirname = "them";
    keys64 = genkey();
    xhttp.open("POST", host, true);
    xhttp.onreadystatechange = getdata_callback;
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    connected = 1;
    xhttp.send("key=" + keys64);
    if (debug) {
      WriteData("DEBUG1: <to relay>   key=" + keys64, 4);
    }
  } else {
    disconnect();
  }
}

function disconnect() {
  queue = [];
  clearTimeout(polling);
  clearTimeout(pollingQueue);
  xhttp.abort();
  WriteData("Disconnected");
  $("send").disabled = true;
  $("send").style.backgroundColor = "#888";
  $("connect").value = "Connect";
  $("signingkey").disabled = false;
  $("signingkey")[0].selected = "selected";
  window_closing();
  connected = 0;
  mefirst = 0;
}

function getdata() {
  xhttp.open("POST", host, true);
  xhttp.onreadystatechange = getdata_callback;
  xhttp.send("p=");
  if (debug >= 6) {
    WriteData("DEBUG6: <to relay>   p=", 4);
  }
}

function getdata_callback() {
  if (xhttp.readyState == 4) {
    var response;
    if (connected && xhttp.responseText != "") {
      response = xhttp.responseText.split("\n");
      handle_response(response);
    }
    if (connected == 2) {
      polling = setTimeout(getdata, 500);
    }
  }
}

function handle_response(res) {
  var done = false;
  for (var i=0; i<res.length; i++) {
    if (res[i] != "") {
      if (debug) {
        WriteData("DEBUG1: <from relay> " + res[i], 4);
      }
      if (res[i].substr(0, 4) == "key=") {
        handle_key(Base64.atob(res[i].substr(4)));
      } else if (res[i].substr(0, 1) == "!") {
        switch(res[i].substr(1)) {
          case "0":
            WriteData("<relay> Other client disconnected.", 3);
            connected = 0;
            done = true;
            disconnect();
            break;
          case "1":
            WriteData("<relay> Room created.  Waiting on second user.", 3);
            mefirst = 1;
            relayConnected();
            break;
          default:
            WriteData("<relay> " + res[i].substr(a), 3);
        }
        if (done) {
          break;
        }
      } else {
        var encryptedBytes = Base64.atob(res[i]);
        var mac = encryptedBytes.splice(encryptedBytes.length-16, 16);
        var decryptedBytes = aead_decrypt(shared_key, theirnonce, encryptedBytes, theiraad, mac, theircounter);
        theircounter += 2 + Math.floor(encryptedBytes.length/64);
        if (decryptedBytes === false) {
          WriteData('Encryption failed.  MAC does not match.');
        } else {
          var printme = encodeUTF8(decryptedBytes);
          if (printme.substring(0, 5) == "!key=") {
            var oldmyname = myname;
            if (!rekeying) {
              clearTimeout(pollingQueue);
              WriteData("Rekey requested.");
              oldkeys[0] = mynonce.slice();
              oldkeys[1] = myaad.slice();
              oldkeys[2] = mycounter;
              rekeying = true;
              keys64 = genkey();
              queue.push("!key=" + keys64);
              senddata();
            }
            clearoldkeys();
            handle_key(Base64.atob(printme.substr(5)));
            myname = oldmyname;
            rekeying = false;
          } else {
            if (printme.substring(0, 2) == "\\\\" || printme.substring(0, 2) == "\\!") {
              printme = printme.substring(1);
            }
            WriteData("<" + theirname + "> " + printme, 2, true);
          }
        }
      }
    }
  }
}

function relayConnected() {
  clearTimeout(polling);
  clearTimeout(pollingQueue);
  polling = setTimeout(getdata, 500);
  pollingQueue = setTimeout(senddata, 1000);
  $("connect").value = "Disconnect";
  $("chat").focus();
  connected = 2;
}

function senddata() {
  if (queue.length > 0) {
    var data = "";
    for (var i=0; i<queue.length; i++) {
      if (rekeying) {
        // Use old key until new key is calculated.
        var encryptedBytes = aead_encrypt(shared_key, oldkeys[0], decodeUTF8(queue[i]), oldkeys[1], oldkeys[2]);
        oldkeys[2] += 2 + Math.floor(encryptedBytes[0].length/64);
      } else {
        var encryptedBytes = aead_encrypt(shared_key, mynonce, decodeUTF8(queue[i]), myaad, mycounter);
        mycounter += 2 + Math.floor(encryptedBytes[0].length/64);
      }
      var encrypted = new Uint8Array(encryptedBytes[0].length + encryptedBytes[1].length);
      encrypted.set(encryptedBytes[0]);
      encrypted.set(encryptedBytes[1], encryptedBytes[0].length);
      var encryptedBytes64 = Base64.btoa(encrypted);
      data = data + "&m[]=" + encryptedBytes64;
    }
    queue = [];
    httpQueue.open("POST", host);
    httpQueue.onreadystatechange = senddata_callback;
    httpQueue.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    httpQueue.send(data.substr(1));
    if (debug) {
      WriteData("DEBUG1: <to relay>   " + data.substr(1), 4);
    }
  } else {
    pollingQueue = setTimeout(senddata, 1000);
  }
}

function senddata_callback() {
  // httpQueue.* will call setTimeout twice.  Using this.* fixes it.
  // I think the status changes before it is read the first time.
  if (this.readyState == 4 && this.status == 200) {
    pollingQueue = setTimeout(senddata, 1000);
  }
}

function send_onClick() {
  if ($("chat").value != "") {
    var sendme = $("chat").value;
    WriteData("<" + myname + "> " + sendme, 1, true);
    if (sendme.substring(0, 1) == "\\" || sendme.substring(0, 1) == "!") {
      sendme = "\\" + sendme;
      if (debug) {
        WriteData("DEBUG1: Prepending escape character.", 4);
      }
    }
    queue.push(sendme);
    $("chat").value = "";
    $("chat").focus();
  }
}

function openKeyManager() {
  $("keymanager").style.display = "";
}

function closeKeyManager() {
  $("keymanager").style.display = "none";
  writeJSON();
}

function addpubkey_onClick() {
  addpublic($("pubname").value, $("pubkey").value);
  $("pubname").value = "";
  $("pubkey").value = "";
  highlightsave();
}

function readINI() {
  $("file").click();
}

function loadINI(e) {
  var file = window.event.target.files[0];
  var reader = new FileReader();
  reader.onload = (function(theFile) {
    return function(e) {
      if (e.target.result[0] == '{') {
        try {
          var data = JSON.parse(e.target.result);
        } catch(e) {
          WriteData("Failed to import keys: " + e);
          return;
        }
        decodeINI(data);
      } else {
        newdecodeINI(e.target.result);
      }
    };
  })(file);
  reader.readAsText(file);
}

function loadJSON() {
  var localStorage;
  try {
    localStorage = window.localStorage;
  } catch(e) {
    return;
  }
  if (localStorage.hasOwnProperty("keys")) {
    var data = JSON.parse(localStorage.getItem("keys"));
    if (data) {
      decodeINI(data);
    }
  }
}

function decodeINI(data) {
  if (data.hasOwnProperty("Private")) {
    for (var key in data["Private"]) {
      addprivate(data["Private"][key][0], key, data["Private"][key][1]);
    }
  } else {
    WriteData("No private keys found.");
  }
  if (data.hasOwnProperty("Public")) {
    for (var key in data["Public"]) {
      addpublic(data["Public"][key], key);
    }
  } else {
    WriteData("No public keys found.");
  }
  needupdate = 0;
  if (data.hasOwnProperty("MrBlue")) {
    if (data["MrBlue"]["iniversion"] < 2) {
      needupdate = 1;
    }
  } else {
    needupdate = 1;
  }
  if (needupdate) {
    updateini();
    writeJSON();
    WriteData("Local keys updated.  Export keys to save a backup.");
  }
}

function newdecodeINI(data) {
  ini = data.match(/[^\r\n]+/g);
  var i, section, values;
  for (i=0; i<ini.length; i++) {
    if (ini[i].charAt(0) == "[") {
      section = ini[i].slice(1, -1);
    } else {
      values = ini[i].split("=");
      switch (section) {
        case "Public":
          addpublic(values[0].trim(), values[1].trim());
          break;
        case "Private":
          var ids = values[1].trim().split(",");
          addprivate(values[0].trim(), ids[1], ids[0]);
          break;
        case "MrBlue":
          switch (values[0].trim()) {
            case "iniversion":
              if (values[1].trim() > 1) {
                needupdate = 0;
              }
              break;
            case "host":
              defaulthost = values[1].trim();
              break;
          }
          break;
      }
    }
  }
}

function writeINI() {
  text = "[Private]\r\n";
  for (var id in privkeys) {
    text += privkeys[id][0] + "=" + privkeys[id][1] + "," + id + "\r\n";
  }
  text += "\r\n[Public]\r\n";
  for (var id in pubkeys) {
    text += pubkeys[id] + "=" + id + "\r\n";
  }
  text += "\r\n[MrBlue]\r\n";
  text += "iniversion=" + iniversion + "\r\n";
  if (defaulthost != "") {
    text += "host=" + defaulthost + "\r\n";
  }

  var a = $("a");
  a.href = "data:text/plain;charset=utf-u,"+encodeURIComponent(text);
  a.download = inifile;
  a.target = "_self";
  a.click();
  $("save").style.backgroundColor = "#000";
}

function writeJSON() {
  var localStorage, name, select = $("signingkey");
  try {
    localStorage = window.localStorage;
  } catch(e) {
    return;
  }
  var blueinfo = {"iniversion": iniversion, "host": defaulthost}
  for (var id in privkeys) {
    name = $("priv" + id).value.trim();
    getOptionByValue(select, id).text = name;
    privkeys[id][0] = name;
  }
  for (var id in pubkeys) {
    name = $("pub" + id).value.trim();
    pubkeys[id] = name;
  }
  var keys = {Private: privkeys, Public: pubkeys, MrBlue: blueinfo};
  var text = JSON.stringify(keys);
  try {
    localStorage.setItem('keys', text);
  } catch(e) {
    return;
  }
}

function updateini() {
  WriteData("Updating legacy passwords");
  op = [0x69, 0x21, 0x7a, 0x30, 0x79, 0x90, 0x80, 0x94, 0xe1, 0x11, 0x21, 0xd0, 0x42, 0x35, 0x4a, 0x7c, 0x1f, 0x55, 0xb6, 0x48, 0x2c, 0xa1, 0xa5, 0x1e, 0x1b, 0x25, 0xd, 0xfd];
  for (var id in privkeys) {
    var skey = inichangepass(op, privkeys[id][1]);
    if (skey !== false) {
      var newskey = encryptSigningKey("", skey);
      for (i=0; i<skey.length; i++) {
        skey[i] = 0;
      }
      privkeys[id][1] = newskey;
    }
  }
}

function inichangepass(key, enc64) {
  var bytes = Base64.atob(enc64);
  var nonce = bytes.splice(0, 12);
  var mac   = bytes.splice(bytes.length-16, 16);
  return aead_decrypt(key, nonce, bytes, signingaad, mac, nonce[3]);
}

function addpublic(name, id) {
  pubkeys[id] = name;
  var mypubcounter = pubcounter;
  var row = $("publickeys").insertRow(-1);
  row.id = "prow" + pubcounter;
  var cell = row.insertCell(0);
  var node = document.createElement("input");
  node.type = "text";
  node.size = "12";
  node.value = name;
  node.id = "pub" + id;
  node.className = "keymanager";
  node.onkeyup = highlightsave;
  cell.appendChild(node);
  cell = row.insertCell(1);
  node = document.createTextNode(id);
  cell.appendChild(node);
  cell = row.insertCell(2);
  node = document.createElement("input");
  node.type = "button";
  node.value = "X";
  node.onclick = function() {
    delpub(id, mypubcounter);
  };
  cell.appendChild(node);
  pubcounter++;
}

function addprivate(name, id, private) {
  privkeys[id] = [name, private];
  var myprivcounter = privcounter;
  var row = $("privatekeys").insertRow(-1);
  row.id = "srow" + privcounter;
  var cell = row.insertCell(0);
  var node = document.createElement("input");
  node.type = "text";
  node.size = "12";
  node.value = name;
  node.id = "priv" + id;
  node.className = "keymanager";
  node.onkeyup = highlightsave;
  cell.appendChild(node);
  cell = row.insertCell(1);
  node = document.createTextNode(id);
  cell.appendChild(node);
  cell = row.insertCell(2);
  node = document.createElement("input");
  node.type = "button";
  node.value = "X";
  node.onclick = function() {
    delpriv(id, myprivcounter);
  };
  cell.appendChild(node);
  cell = row.insertCell(3);
  node = document.createElement("input");
  node.type = "button";
  node.value = "Change";
  node.id = "cs" + privcounter;
  node.onclick = function() {
    changepasswrapper(id);
  };
  cell.appendChild(node);
  node = document.createElement("option");
  node.value = id;
  node.text = name;
  $("signingkey").add(node);
  privcounter++;
}

function delpub(id, counter) {
  delete pubkeys[id];
  var row = $("prow" + counter);
  row.parentNode.removeChild(row);
  highlightsave();
}

function delpriv(id, counter) {
  delete privkeys[id];
  var row = $("srow" + counter);
  row.parentNode.removeChild(row);
  var select = $("signingkey");
  select.removeChild(getOptionByValue(select, id));
  highlightsave();
}

// https://stackoverflow.com/questions/41112624/remove-select-option-with-specific-value-using-prototype-js/41112759#41112759
function getOptionByValue (select, value) {
  var options = select.options;
  for (var i = 0; i < options.length; i++) {
    if (options[i].value === value) {
      return options[i];
    }
  }
  return null;
}

function generatekey() {
  var name = $("keyname").value;
  if ($("keypass").style.display === "none") {
    var pass = $("keypassclear").value;
  } else {
    var pass = $("keypass").value;
  }
  $("keyname").value = "";
  $("keypass").value = "";
  $("keypassclear").value = "";
  highlightsave();
  if (name == "") name = "default";
  var sk = generateSecret(8);
  var pk = getpublickey(sk);
  addprivate(name, Base64.btoa(pk), encryptSigningKey(pass, sk));
}

function highlightsave() {
  $("save").style.backgroundColor = "#009";
}

function genkey() {
  var rnd = generateSecret(11);
  mynonce = rnd.splice(0,12);
  if (debug >=2) {
    WriteData("DEBUG2: My Nonce:   " + padarray(mynonce, 3), 4);
  }
  secret = [];
  for (i=0; i<rnd.length; i+=2) {
    secret.push(U8TO16_LE(rnd, i));
  }
  if (debug >=5) {
    WriteData("DEBUG5: Secret key: " + padarray(secret, 5), 4);
  }
  for (i=0; i<rnd.length; i++) {
    rnd[i] = 0;
  }
  public_key = curve25519(secret);
  public_key32 = c255lbase32encode(public_key);
  keys8 = mynonce.slice(0);
  var start = keys8.length;
  for (i=0; i<public_key.length; i++) {
    U16TO8_LE(keys8, start+i*2, public_key[i]);
  }
  myaad = aad.slice();
  if (signingkey !== false) {
    var select = $("signingkey");
    myname = select.options[select.selectedIndex].text;
    WriteData("Setting your name to " + myname + ".");
    myaad[11]++;
    if (debug >= 2) {
      WriteData("DEBUG2: Changing my aad to " + myaad, 4);
    }
    var pk = Base64.atob(select.value);
    var signature = sign(keys8, signingkey, pk);
    var keys64 = Base64.btoa(pk.concat(signature.concat(keys8)));
    select[0].selected = "selected";
    for (i=0; i<signingkey.length; i++) {
      signingkey[i] = 0;
    }
    signingkey = false;
  } else {
    myname = "me";
    var keys64 = Base64.btoa(keys8);
  }
  WriteData("Your Public Key:  " + public_key32);
  return keys64;
}

function handle_key(keys) {
  theiraad = aad.slice();
  // Signed key
  if (keys.length == 140) {
    var pk = keys.splice(0, 32);
    if (debug) {
      WriteData("DEBUG1: Found signing key " + Base64.btoa(pk), 4);
    }
    theiraad[11]++;
    if (debug >= 2) {
      WriteData("DEBUG2: Changing their aad to " + theiraad, 4);
    }
    var s = keys.splice(0, 64);
    if (verify(keys, pk, s)) {
      WriteData("Valid signature");
    } else {
      WriteData("Invalid signature");
      connected = 2;
      disconnect();
      return;
    }
    var b64pk = Base64.btoa(pk);
    if (pubkeys[b64pk] !== undefined) {
      theirname = pubkeys[b64pk];
      WriteData("Setting their name to " + theirname + ".");
    } else {
      WriteData("Unknown signing key: " + b64pk);
    }
  }
  if (keys.length != 44) {
    WriteData("Unknown key format");
    disconnect();
    return;
  }
  theirnonce = keys.splice(0, 12);
  // force some distance between nonces.
  if (debug >= 2) {
    WriteData("DEBUG2: My Nonce:            " + padarray(mynonce, 3), 4);
    WriteData("DEBUG2: Their Nonce:         " + padarray(theirnonce, 3), 4);
  }
  if (mefirst) {
    mynonce[8]    |= 0x1;
    theirnonce[8] &= 0xfe;
  } else {
    mynonce[8]    &= 0xfe;
    theirnonce[8] |= 0x1;
  }
  if (debug >= 2) {
    WriteData("DEBUG2: Updated My Nonce:    " + padarray(mynonce, 3), 4);
    WriteData("DEBUG2: Updated Their Nonce: " + padarray(theirnonce, 3), 4);
  }
  theirpublic = [];
  for (i=0; i<keys.length; i+=2) {
    theirpublic.push(U8TO16_LE(keys, i));
  }
  var theirpublic32 = c255lbase32encode(theirpublic);
  WriteData("Their Public Key: " + theirpublic32);
  var shared_key16 = curve25519(secret, theirpublic);
  for (i=0; i<secret.length; i++) {
    secret[i] = 0;
  }
  secret = false;
  shared_key = [];
  for (i=0; i<shared_key16.length; i++) {
    U16TO8_LE(shared_key, i*2, shared_key16[i]);
    shared_key16[i] = 0;
  }
  shared_key16 = false;
  var h = new BLAKE2s(32);
  h.update(shared_key);
  if (debug >= 5) {
    WriteData("DEBUG5: Shared key: " + padarray(shared_key, 3), 4);
    WriteData("DEBUG5: Hashed key: " + padarray(h.digest(), 3), 4);
  }
  shared_key = h.digest();
  h.digest(generateSecret(31));
  h = false;
  mycounter = 0;
  theircounter = 0;
  $("send").disabled = false;
  $("send").style.backgroundColor = '#009';
  $("rekey").disabled = false;
  WriteData("Encryption Ready");
  relayConnected();
}

function rekeyConnection() {
  if (rekeying) return;
  if (connected == 2) {
    var oldmyname = myname;
    WriteData("Rekey Initiated.");
    $("rekey").disabled = true;
    $("send").disabled = true;
    $("send").style.backgroundColor = "#888";
    rekeying = true;
    oldkeys[0] = mynonce.slice();
    oldkeys[1] = myaad.slice();
    oldkeys[2] = mycounter;
    keys64 = genkey();
    queue.push("!key=" + keys64);
    myname = oldmyname;
  } else {
    WriteData("Not connected.");
  }
}

function clearoldkeys() {
  if (oldkeys !== undefined && oldkeys.length > 1) {
    for (var i=0; i<oldkeys.length; i++) {
      if (Array.isArray(oldkeys[i])) {
        for (var j=0; j<oldkeys[i].length; j++) {
          oldkeys[i][j] = 0;
        }
      } else {
        oldkeys[i] = 0;
      }
    }
  }
  oldkeys = [];
}

// ed25519 logic from https://github.com/mpaland/mipher
// https://github.com/mpaland/mipher/blob/master/src/x25519.ts
function getpublickey(sk, h) {
  var p = [gf(), gf(), gf(), gf()],
      pk = Array(32);

  if (!h) {
    var h = Array(64);
    crypto_hash(h, sk, 32);

    h[0]  &= 0xf8;
    h[31] = (h[31] & 0x7f) | 0x40;
  }

  // compute pk
  scalarbase(p, h.slice(0, 32));
  pack(pk, p);

  return pk;
}

function sign(message, sk, pk) {
  var p = [gf(), gf(), gf(), gf()];

  var h = Array(64);
  crypto_hash(h, sk, 32);

  h[0]  &= 0xf8;
  h[31] = (h[31] & 0x7f) | 0x40;

  if (pk.length !== 32) {
    pk = getpublickey(sk, h);
  }

  // compute r = SHA512(h[32-63] || M)
  var s = Array(32);
  var r = Array(64);
  crypto_hash(r, h.slice(32).concat(message), message.length+32);
  reduce(r);
  scalarbase(p, r);
  pack(s, p);

  // compute k = SHA512(R || A || M)
  var k = Array(64);
  crypto_hash(k, s.concat(pk.concat(message)), message.length+64);
  reduce(k);

  // compute s = (r + k a) mod q
  var i, j
  var x = r.slice(0, 32).concat([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
  for (i=0; i<32; i++) {
    for (j=0; j<32; j++) {
      x[i+j] += k[i] * h[j];
    }
  }
  modL(s.slice(32), x);

  return s.concat(x.slice(0,32));
}

function verify(message, pk, s) {
  var p = [gf(), gf(), gf(), gf()],
      q = [gf(), gf(), gf(), gf()];

  if (s.length !== 64) return false;
  if (pk.length !== 32) return false;
  if (unpackNeg(q, pk)) return false;

  var r = s.splice(0, 32);
  // compute k = SHA512(R || A || M)
  var k = Array(64);
  crypto_hash(k, r.concat(pk.concat(message)), message.length+64);
  reduce(k);
  scalarmult(p, q, k);

  var t = new Array(64);
  scalarbase(q, s);
  add(p, q);
  pack(t, p);
  t = t.slice(0,32);

  return compare(t, r);
}

function encryptSigningKey(password, sk) {
  var key       = hashpass(password);
  var nonce     = generateSecret(3);
  var bytes     = aead_encrypt(key, nonce, sk, signingaad, nonce[3]);
  var out = nonce.slice();
  for (var i=0; i<bytes[0].length; i++) {
    out.push(bytes[0][i]);
  }
  for (i=0; i<bytes[1].length; i++) {
    out.push(bytes[1][i]);
  }
//  return Base64.btoa(nonce.concat(bytes[0].concat(bytes[1])));
  return Base64.btoa(out);
}

function decryptSigningKey(password, enc64) {
  var key   = hashpass(password);
  var bytes = Base64.atob(enc64);
  var nonce = bytes.splice(0, 12);
  var mac   = bytes.splice(bytes.length-16, 16);
  return aead_decrypt(key, nonce, bytes, signingaad, mac, nonce[3]);
}

function hashpass(password) {
  var key;
  if (password !== "") {
    var h = new BLAKE2s(32);
    h.update(decodeUTF8(password));
    key = h.digest();
  } else {
    // Hard-code blank password, don't waste time calculating it.
    key = [0x69, 0x21, 0x7a, 0x30, 0x79, 0x90, 0x80, 0x94, 0xe1, 0x11, 0x21, 0xd0, 0x42, 0x35, 0x4a, 0x7c, 0x1f, 0x55, 0xb6, 0x48, 0x2c, 0xa1, 0xa5, 0x1e, 0x1b, 0x25, 0xd, 0xfd, 0x1e, 0xd0, 0xee, 0xf9];
  }
  return key;
}

function keychange() {
  var select = $("signingkey");
  if (select.value != "") {
    // Try to decrypt without a password, then prompt
    signingkey = decryptSigningKey("", privkeys[select.value][1]);
    if (signingkey === false) {
      $("passwordPrompt").style.display = "";
      $("askpass").focus();
    } else {
      WriteData("Using signing key: " + select.value);
    }
  } else {
    signingkey = false;
  }
}

function trypass() {
  var select = $("signingkey");
  signingkey = decryptSigningKey($("askpass").value, privkeys[select.value][1]);
  $("askpass").value = "";
  if (signingkey === false) {
    select[0].selected = "selected";
    signingkey = false;
    WriteData("Failed to decrypt key.");
  } else {
      WriteData("Using signing key: " + select.value);
  }
  $("passwordPrompt").style.display = "none";
}

function passkeyup() {
  if (window.event.keyCode === 13) {
    trypass();
  }
}

function togglepass() {
  var n = $("keypass");
  var o = $("keypassclear");
  if (o.style.display === "none") {
    o.value = n.value;
    n.style.display = "none";
    o.style.display = "";
    o.focus();
    var val = o.value;
    o.value = "";
    o.value = val;
  } else {
    n.value = o.value;
    o.style.display = "none";
    n.style.display = "";
    n.focus();
    var val = n.value;
    n.value = "";
    n.value = val;
  }
}

function togglechangepass() {
  var n = $("oldpass");
  var o = $("oldpassclear");
  var p = $("newpass1");
  var q = $("newpass1clear");
  var r = $("newpass2");
  var s = $("newpass2clear");
  if (o.style.display === "none") {
    o.value = n.value;
    q.value = p.value;
    s.value = r.value;
    n.style.display = "none";
    p.style.display = "none";
    r.style.display = "none";
    o.style.display = "";
    q.style.display = "";
    s.style.display = "";
  } else {
    n.value = o.value;
    p.value = q.value;
    r.value = s.value;
    o.style.display = "none";
    q.style.display = "none";
    s.style.display = "none";
    n.style.display = "";
    p.style.display = "";
    r.style.display = "";
  }
}

function changepass(id) {
  if ($("oldpass").style.display === "none") {
    var old  = $("oldpassclear");
    var new1 = $("newpass1clear");
    var new2 = $("newpass2clear");
  } else {
    var old  = $("oldpass");
    var new1 = $("newpass1");
    var new2 = $("newpass2");
  }
  if (new1.value !== new2.value) {
    WriteData("New Passwords do not match.");
  } else {
    var skey = decryptSigningKey(old.value, privkeys[id][1]);
    if (skey === false) {
      WriteData("Old Password is incorrect.");
    } else {
      var newskey = encryptSigningKey(new1.value, skey);
      for (i=0; i<skey.length; i++) {
        skey[i] = 0;
      }
      privkeys[id][1] = newskey;
      highlightsave();
      WriteData("Password changed.");
    }
  }
  $("changePass").style.display = "none";
  $("oldpass").value       = "";
  $("newpass1").value      = "";
  $("newpass2").value      = "";
  $("oldpassclear").value  = "";
  $("newpass1clear").value = "";
  $("newpass2clear").value = "";
}

function changepasswrapper(id) {
  $("passchangeok").onclick = function() {
    changepass(id);
  };
  $("changePass").style.display = "";
}

function generateSecret(words) {
  var ints = new Uint32Array(words), rnd = [], keys = [];
  var crypto = window.crypto || window.msCrypto;
  try {
    crypto.getRandomValues(ints);
  }
  catch(error) {
    WriteData("window.crypto is missing.  Unable to continue.");
    exit;
  }
  for (var i=0; i<words; i++) {
    U32TO8_BE(keys, 4*i, ints[i]);
    ints[i] = 0;
  }
  return keys;
}

function showHelp() {
  WriteData("");
  WriteData("Quick help:");
  WriteData("");
  WriteData("Keys:");
  WriteData("Click the \"Keys\" button to open the Key Manager.");
  WriteData("Type a Display Name and optionally a Password and click \"Generate Signing Key\".");
  WriteData("Your Display Name is displayed in the chat window when you type.  Your name is a good choice here.");
  WriteData("Send the public key to your chatting partner.");
  WriteData("Ask your chatting partner to send their public key to you.");
  WriteData("Add their name and public key under the \"Known public keys\" section and click \"Add\".");
  WriteData("This Display Name will display in the chat window when they type.  Their name is a good choice here.");
  WriteData("Close the Key Manager.");
  WriteData("");
  WriteData("Connecting:");
  WriteData("Click \"Select Key\" and select your private key.");
  WriteData("If you password protected the key you will be prompted for the password.");
  WriteData("You can have more than one private key.");
  WriteData("Type a unique URL in the URL bar, or the \"Relay URL\" text input, known only to you and your chatting partner.");
  WriteData("Click Connect.");
  WriteData("");
  WriteData("Chatting:");
  WriteData("The first to arrive will be told to wait.");
  WriteData("Once you are both connected you will both see \"Encryption Ready\".");
  WriteData("Type a message and press enter or click the \"Send\" button.");
  WriteData("The message will be encrypted with your shared key before being sent.");
  WriteData("Click \"Disconnect\" or close the browser window to end the chat session.");
  WriteData("");
  WriteData("For more information visit https://bitbucket.org/jzielke/mr-blue/")
  WriteData("");
}

// https://gist.github.com/jonleighton/958841#gistcomment-2839519
Base64 = {

  _chars :
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_",

  btoa : function(arraybuffer) {
    var bytes = new Uint8Array(arraybuffer),
      i, len = bytes.length, base64 = [];

    for (i = 0; i < len; i+=3) {
      base64.push(this._chars[bytes[i] >> 2]);
      base64.push(this._chars[((bytes[i] & 3) << 4) | (bytes[i + 1] >> 4)]);
      base64.push(this._chars[((bytes[i + 1] & 15) << 2) | (bytes[i + 2] >> 6)]);
      base64.push(this._chars[bytes[i + 2] & 63]);
    }
    base64 = base64.join("");

    // Do not add padding for transport
    if ((len % 3) === 2) {
      base64 = base64.substring(0, base64.length - 1);
    } else if (len % 3 === 1) {
      base64 = base64.substring(0, base64.length - 2);
    }

    return base64;
  },

  atob : function(base64) {
    // Add padding
    while(base64.length % 4 != 0) {
      base64 += ".";
    }

    var bufferLength = base64.length * 0.75,
    len = base64.length, i, p = 0,
    encoded1, encoded2, encoded3, encoded4;

    // Use a lookup table to find the index.
    var lookup = new Uint8Array(256);
    for (var i = 0; i < this._chars.length; i++) {
      lookup[this._chars.charCodeAt(i)] = i;
    }

    if (base64[base64.length - 1] === ".") {
      bufferLength--;
      if (base64[base64.length - 2] === ".") {
        bufferLength--;
      }
    }

      bytes = new Array(bufferLength);

    for (i = 0; i < len; i+=4) {
      encoded1 = lookup[base64.charCodeAt(i)];
      encoded2 = lookup[base64.charCodeAt(i+1)];
      encoded3 = lookup[base64.charCodeAt(i+2)];
      encoded4 = lookup[base64.charCodeAt(i+3)];

      bytes[p++] = (encoded1 << 2) | (encoded2 >> 4);
      if (p >= bufferLength) break;
      bytes[p++] = ((encoded2 & 15) << 4) | (encoded3 >> 2);
      if (p >= bufferLength) break;
      bytes[p++] = ((encoded3 & 3) << 6) | (encoded4 & 63);
    }

    return bytes;
  }
}

// https://stackoverflow.com/questions/29867862/how-to-get-current-time-in-a-format-hhmm-am-pm-in-javascript/41734080
function getTime() {
  var d = new Date();
  return (
    (d.getHours() < 10 ? '0' : '') + d.getHours() +
    (d.getMinutes() < 10 ? ':0' : ':') + d.getMinutes() +
    (d.getSeconds() < 10 ? ':0' : ':') + d.getSeconds()
  );
}

function U8TO16_BE(x, i) {
  return (x[i]<<8) | x[i+1];
}

function U16TO8_BE(p, pos, v) {
    p[pos]   = (v >> 8) & 0xff;
    p[pos+1] = (v     ) & 0xff;
}

function U32TO8_BE(p, pos, v) {
    p[pos]   = (v >> 24) & 0xff;
    p[pos+1] = (v >> 16) & 0xff;
    p[pos+2] = (v >>  8) & 0xff;
    p[pos+3] = (v      ) & 0xff;
}

function WriteData(data, color, enc) {
  enc = typeof enc !== 'undefined' ? enc : false;
  color = typeof color !== "undefined" ? color : 0;
  var timestamp = getTime();
  var sp = " ";
  if (enc) {
    sp = "_";
  }
  $("htmltextarea").innerHTML += "<span style=\"color: " + msgcolor[color] + "\">" + HTMLescape(timestamp + sp + data) + "</span><br>";
  scrolldown();
}

// https://stackoverflow.com/a/17546215/698168
function HTMLescape(html){
  DOMtext.nodeValue = html;
  return DOMnative.innerHTML
}

function padarray(arr, width) {
  var i, n, out = "";
  for (i=0; i<arr.length; i++) {
    n = arr[i].toString();
    while (n.length<width) n = "0" + n;
    out += n + ",";
  }
  return out.substring(0, out.length-1);
}

function encodeUTF8(arr) {
  var i, b = [];
  for (i=0; i<arr.length; i++) b.push(String.fromCharCode(arr[i]));
  return decodeURIComponent(escape(b.join("")));
}

